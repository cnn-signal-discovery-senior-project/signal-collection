import os
from warnings import filters
from numpy.core.numeric import ones
import yaml
import sys
import time
import numpy as np
from math import ceil
import tensorflow as tf
import matplotlib
matplotlib.use('QT5Agg')

import utils.signal_processing as sig_proc
from radios import radio_sim
try:
    from radios.radio import radio
except:
    print("can't run real radios as packages aren't installed")

from matplotlib.backends.qt_compat import QtWidgets
from matplotlib.backends.backend_qt5agg import (
    FigureCanvas, NavigationToolbar2QT as NavigationToolbar)
from matplotlib.figure import Figure
from PyQt5 import QtCore, QtGui, QtWidgets

class ApplicationWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self._main = QtWidgets.QWidget()
        self.setCentralWidget(self._main)

        # read in the config for the application
        base_dir = os.path.dirname(os.path.abspath(__file__)) # directory of the experiment
        config_file = open(os.path.join(base_dir, 'config','sig_disc_config.yaml'))
        self.params = yaml.safe_load(config_file)
        
        # create radio interface
        self.radio = None
        if self.params['sim_signal']:
            self.radio = radio_sim.Simradio(numSamps=self.params['samp_buffer'], sig_file=self.params['sig_file'])
        else:
            self.radio = radio.radio(bsnodes=self.params['bsnode'],numSamps=self.params['samp_buffer'])

        # load in tensorflow NN model
        self.model = tf.keras.models.load_model(self.params["CNN_model"])

        self.kernals = self._generate_kernals()
        self.keranal_plot_cols = 4
        self.keranal_plot_rows = 2

        
        # specify number of feature plots per column and row
        self.fm_plot_cols = 1
        self.fm_plot_rows = 4

        # setup model for feature maps
        feature_layers = self.model.layers[1].output
        for i in range(2,10):
           feature_layers = self.model.layers[i](feature_layers)
        self.fm_model = tf.keras.models.Model(inputs=self.model.input, outputs=feature_layers)
        self.fms = np.ones((feature_layers.shape[1], feature_layers.shape[2]))/2
        self._fm_sample_space = np.arange(0, feature_layers.shape[1], step=1)
        
        # setup the ui
        self.setupUi(self)

    def _update_canvas(self):
        t = np.linspace(0, 10, 101)
        # Shift the sinusoid as a function of time.
        self._line.set_data(t, np.sin(t + time.time()))
        self._line.figure.canvas.draw()
        

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(930, 682)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")

        ##################################################
        # setup the tabbed canvase and all of its areas
        ##################################################
        self.sample_canvas = QtWidgets.QTabWidget(self.centralwidget)
        self.sample_canvas.setMinimumSize(QtCore.QSize(438, 438))
        self.sample_canvas.setObjectName("sample_canvas")
        self.horizontalLayout.addWidget(self.sample_canvas)

        #####################################################
        # setup the sampling page
        self.sample_page = QtWidgets.QWidget()
        self.sample_page.setObjectName("sample_page")
        self.sample_canvas.addTab(self.sample_page, "single_samples")

        canvas_layout = QtWidgets.QVBoxLayout()
        td_canvas = FigureCanvas(Figure(figsize=(5, 3)))
        # Ideally one would use self.addToolBar here, but it is slightly
        # incompatible between PyQt6 and other bindings, so we just add the
        # toolbar as a plain widget instead.
        canvas_layout.addWidget(NavigationToolbar(td_canvas, self))
        canvas_layout.addWidget(td_canvas)

        fd_canvas = FigureCanvas(Figure(figsize=(5, 3)))
        canvas_layout.addWidget(fd_canvas)
        canvas_layout.addWidget(NavigationToolbar(fd_canvas, self))

        #setup the plots for td and fd domains
        t = np.linspace(0, 2048)
        # Shift the sinusoid as a function of time.
        self._td_ax = td_canvas.figure.subplots()
        self._td_ax.set_ylabel('Signal (units)')
        self._td_ax.set_xlabel('Sample index')
        self._td_ax.set_ylim([-1, 1])
        self._td_ax.set_xlim([0, 2048])
        self._td_line,  = self._td_ax.plot(t, np.ones(t.shape)/2)

        # create the prediction lines
        self._start_time_line = self._td_line.axes.axvline(0)
        self._stop_time_line = self._td_line.axes.axvline(0)

        self._fd_ax = fd_canvas.figure.subplots()
        self._fd_ax.title.set_text('PSD of rx signal')
        self._fd_ax.set_ylabel('Signal (dB)')
        self._fd_ax.set_xlabel('frequency [Hz]')
        self._fd_ax.set_ylim([-180, 0])
        self._fd_ax.set_xlim([-1, 1])
        self._fd_line, = self._fd_ax.plot(t, np.ones(t.shape)/2)

        # create the prediction lines
        self._lhs_bw_line = self._fd_line.axes.axvline(0)
        self._rhs_bw_line = self._fd_line.axes.axvline(0)

        self.sample_page.setLayout(canvas_layout)

        #############################################################
        #setup the streaming page
        self.stream = QtWidgets.QWidget()
        self.stream.setObjectName("stream")
        self.sample_canvas.addTab(self.stream, "")

        ##############################################################
        # setup kernal visulaize page
        self.keranls_page = QtWidgets.QWidget()
        self.keranls_page.setObjectName("kernals_page")
        self.sample_canvas.addTab(self.keranls_page, "kernals")

        kernals_layout = QtWidgets.QVBoxLayout()
        kernals_canvas = FigureCanvas(Figure())

        # setup kernal plots
        kernal_fig = kernals_canvas.figure
        self.kernal_axes = kernals_canvas.figure.subplots(nrows=self.keranal_plot_rows, ncols=self.keranal_plot_cols, squeeze=False)
        self.kernal_plots = np.empty(shape=self.kernal_axes.shape, dtype='O')
        for row_idx in range(self.keranal_plot_rows):
            for col_idx in range(self.keranal_plot_cols):
                # get axis for specific kernal
                ax = self.kernal_axes[row_idx,col_idx]

                # plan for the below statement ->  unique frame index in first n kernals
                kernal_idx =  (row_idx+col_idx*self.keranal_plot_rows)
                self.kernal_plots[row_idx,col_idx] = ax.imshow(self.kernals[:,:,kernal_idx], cmap='hot',interpolation='nearest', vmin=0, vmax=1)
        # add colorbar to teh right of the plots
        kernal_fig.subplots_adjust(right=0.8)
        color_bar_ax = kernal_fig.add_axes([0.85, 0.15, 0.05, 0.7])
        kernal_fig.colorbar(self.kernal_plots[0,0], cax=color_bar_ax)   

        kernals_layout.addWidget(NavigationToolbar(kernals_canvas, self))
        kernals_layout.addWidget(kernals_canvas)

        # create a slider to change which kernal you are looking at
        kernal_slider = QtWidgets.QSlider(QtCore.Qt.Horizontal)
        kernal_slider.setMinimum(0)
        num_panels = int(ceil(self.kernals.shape[2]/(self.keranal_plot_cols*self.keranal_plot_rows)))
        kernal_slider.setMaximum(num_panels)
        kernal_slider.setValue(0)
        kernal_slider.valueChanged.connect(self._update_displayed_kernals)
        kernals_layout.addWidget(kernal_slider)

        # add layout to the kernals page
        self.keranls_page.setLayout(kernals_layout)

        ########################################################
        # add a feature maps (fm) visualization page
        self.fm_page = QtWidgets.QWidget()
        self.fm_page.setObjectName("fm_page")
        self.sample_canvas.addTab(self.fm_page, "feature maps")

        fm_layout = QtWidgets.QVBoxLayout()
        fm_canvas = FigureCanvas(Figure())

        # setup kernal plots
        fm_fig = fm_canvas.figure
        self.fm_axes = fm_canvas.figure.subplots(nrows=self.fm_plot_rows, ncols=self.fm_plot_cols, squeeze=False, sharex= 'all', sharey='all')
        self.fm_plots = np.empty(shape=self.fm_axes.shape, dtype='O')
        for row_idx in range(self.fm_plot_rows):
            for col_idx in range(self.fm_plot_cols):
                # get axis for specific kernal
                ax = self.fm_axes[row_idx,col_idx]

                # plan for the below statement ->  unique frame index in first n kernals
                fm_idx =  (row_idx+col_idx*self.fm_plot_rows)

                ax.set_ylim([-1,1])
                ax.set_xlim([0, self._fm_sample_space[-1]])
                self.fm_plots[row_idx,col_idx] = ax.plot(self._fm_sample_space, self.fms[:,fm_idx])[0] # returns a list so just grab the first one
  
        fm_fig.supylabel('Signal (units)')
        fm_fig.supxlabel('Samples (time)')

        fm_layout.addWidget(NavigationToolbar(fm_canvas, self))
        fm_layout.addWidget(fm_canvas)

        # create a slider to change which kernal you are looking at
        self.fm_slider = QtWidgets.QSlider(QtCore.Qt.Horizontal)
        self.fm_slider.setMinimum(0)
        num_panels = int(ceil(self.kernals.shape[2]/(self.fm_plot_cols*self.fm_plot_rows)))
        self.fm_slider.setMaximum(num_panels)
        self.fm_slider.setValue(0)
        self.fm_slider.valueChanged.connect(self._update_displayed_fms)
        fm_layout.addWidget(self.fm_slider)

        # add layout to teh kernals page
        self.fm_page.setLayout(fm_layout)

        ##############################################################
        # setup a model summary page
        
        self.model_summary = QtWidgets.QWidget()
        self.model_summary.setObjectName("model_summary")
        self.sample_canvas.addTab(self.model_summary, "CNN model summary")
        
        ms_layout = QtWidgets.QVBoxLayout()

        # create the summary label
        ms_label = QtWidgets.QLabel(parent=self.model_summary)

        # get the summary of the model 
        summary_list = []
        self.model.summary(print_fn= lambda x: summary_list.append(x))
        summary_text = "\n".join(summary_list)
        
        # add summary to labe and format it
        ms_label.setText(summary_text)


        ms_layout.addWidget(ms_label)
        self.model_summary.setLayout(ms_layout)


        ##############################################################
        # setup a testing page
        self.test_page = QtWidgets.QWidget()
        self.test_page.setObjectName("test_page")
        self.sample_canvas.addTab(self.test_page, "test_page")

        #creat the matplotlib canvasas for time domain and frequency domain 
        #representations for the captured sample
        canvas_layout = QtWidgets.QVBoxLayout()
        
        # Ideally one would use self.addToolBar here, but it is slightly
        # incompatible between PyQt6 and other bindings, so we just add the
        # toolbar as a plain widget instead.
        static_canvas = FigureCanvas(Figure(figsize=(5, 3)))
        canvas_layout.addWidget(NavigationToolbar(static_canvas, self))
        canvas_layout.addWidget(static_canvas)

        dynamic_canvas = FigureCanvas(Figure(figsize=(5, 3)))
        canvas_layout.addWidget(dynamic_canvas)
        canvas_layout.addWidget(NavigationToolbar(dynamic_canvas, self))

        self._td_ax = static_canvas.figure.subplots()
        t = np.linspace(0, 10, 501)
        self._td_ax.plot(t, np.tan(t), ".")

        self._fd_ax = dynamic_canvas.figure.subplots()
        t = np.linspace(0, 10, 101)
        # Set up a Line2D.
        self._line, = self._fd_ax.plot(t, np.sin(t + time.time()))
        self._timer = dynamic_canvas.new_timer(50)
        self._timer.add_callback(self._update_canvas)
        self._timer.start()

        self.test_page.setLayout(canvas_layout)

        ##########################################################
        # create the options bar 
        #########################################################
        spacerItem = QtWidgets.QSpacerItem(20, 438, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.sample_button = QtWidgets.QPushButton(self.centralwidget)
        self.sample_button.setObjectName("sample_button")
        self.verticalLayout.addWidget(self.sample_button)
        self.stream_button = QtWidgets.QPushButton(self.centralwidget)
        self.stream_button.setObjectName("stream_button")
        self.verticalLayout.addWidget(self.stream_button)
        spacerItem1 = QtWidgets.QSpacerItem(60, 78, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem1)
        self.snr_label = QtWidgets.QLabel(self.centralwidget)
        self.snr_label.setAlignment(QtCore.Qt.AlignCenter)
        self.snr_label.setObjectName("snr_label")
        self.verticalLayout.addWidget(self.snr_label)
        self.snr_out = QtWidgets.QLabel(self.centralwidget)
        self.snr_out.setText("")
        self.snr_out.setAlignment(QtCore.Qt.AlignCenter)
        self.snr_out.setObjectName("snr_out")
        self.verticalLayout.addWidget(self.snr_out)
        self.bw_label = QtWidgets.QLabel(self.centralwidget)
        self.bw_label.setAlignment(QtCore.Qt.AlignCenter)
        self.bw_label.setObjectName("bw_label")
        self.verticalLayout.addWidget(self.bw_label)
        self.bw_out = QtWidgets.QLabel(self.centralwidget)
        self.bw_out.setText("")
        self.bw_out.setAlignment(QtCore.Qt.AlignCenter)
        self.bw_out.setObjectName("bw_out")
        self.verticalLayout.addWidget(self.bw_out)
        self.cf_label = QtWidgets.QLabel(self.centralwidget)
        self.cf_label.setAlignment(QtCore.Qt.AlignCenter)
        self.cf_label.setObjectName("cf_label")
        self.verticalLayout.addWidget(self.cf_label)
        self.cf_out = QtWidgets.QLabel(self.centralwidget)
        self.cf_out.setText("")
        self.cf_out.setAlignment(QtCore.Qt.AlignCenter)
        self.cf_out.setObjectName("cf_out")
        self.verticalLayout.addWidget(self.cf_out)
        self.st_label = QtWidgets.QLabel(self.centralwidget)
        self.st_label.setAlignment(QtCore.Qt.AlignCenter)
        self.st_label.setObjectName("st_label")
        self.verticalLayout.addWidget(self.st_label)
        self.st_out = QtWidgets.QLabel(self.centralwidget)
        self.st_out.setText("")
        self.st_out.setAlignment(QtCore.Qt.AlignCenter)
        self.st_out.setObjectName("st_out")
        self.verticalLayout.addWidget(self.st_out)
        self.et_label = QtWidgets.QLabel(self.centralwidget)
        self.et_label.setAlignment(QtCore.Qt.AlignCenter)
        self.et_label.setObjectName("et_label")
        self.verticalLayout.addWidget(self.et_label)
        self.et_out = QtWidgets.QLabel(self.centralwidget)
        self.et_out.setText("")
        self.et_out.setAlignment(QtCore.Qt.AlignCenter)
        self.et_out.setObjectName("et_out")
        self.verticalLayout.addWidget(self.et_out)
        self.do_label = QtWidgets.QLabel(self.centralwidget)
        self.do_label.setAlignment(QtCore.Qt.AlignCenter)
        self.do_label.setObjectName("do_label")
        self.verticalLayout.addWidget(self.do_label)
        self.doa_out = QtWidgets.QLabel(self.centralwidget)
        self.doa_out.setText("")
        self.doa_out.setAlignment(QtCore.Qt.AlignCenter)
        self.doa_out.setObjectName("doa_out")
        self.verticalLayout.addWidget(self.doa_out)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.gridLayout.addLayout(self.horizontalLayout, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 930, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        # create the conenctions for push button callbacks
        self.sample_button.pressed.connect(self.sample_button_pressed)

        self.retranslateUi(MainWindow)
        self.sample_canvas.setCurrentIndex(1)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.sample_canvas.setTabText(self.sample_canvas.indexOf(self.sample_page), _translate("MainWindow", "single sample"))
        self.sample_canvas.setTabText(self.sample_canvas.indexOf(self.stream), _translate("MainWindow", "stream"))
        self.sample_button.setText(_translate("MainWindow", "Sample"))
        self.stream_button.setText(_translate("MainWindow", "Stream"))
        self.snr_label.setText(_translate("MainWindow", "SNR"))
        self.bw_label.setText(_translate("MainWindow", "Bandwidth"))
        self.cf_label.setText(_translate("MainWindow", "Center Freqquency"))
        self.st_label.setText(_translate("MainWindow", "Start Sample"))
        self.et_label.setText(_translate("MainWindow", "End Sample"))
        self.do_label.setText(_translate("MainWindow", "DoA"))

    def sample_button_pressed(self):
        self._sample_spectrum()

    def _sample_spectrum(self):
        # gather sample from radio
        in_sig, cf, bw = self.radio.rx()

        # preprocess signal to gather freq domain properties
        rx_freq, powerBins, nf, peaks = sig_proc.fft_power(in_sig, self.params['sample_rate'])

        # predict signal properties using tensor flow model
        param_pred = self.model.predict(np.array([in_sig]))
        st_time = param_pred[0][0]
        et_time = param_pred[0][1]

        self._update_sample_plots(in_sig, powerBins, rx_freq, st_time, et_time, cf, bw)

        self._update_sig_param_labels(0,bw,cf,st_time,et_time,0)

        self._update_feature_maps(in_sig)

    def _update_sample_plots(self, td_sig, fd_sig, fd_bins, st_time, et_time, cf, bw):
        t = range(len(td_sig))

        # plot the time domain signal

        self._td_line.set_data(t,td_sig)
        # make a marker for the start and stop times on the time domain plot
        self._start_time_line.remove()
        self._start_time_line = self._td_line.axes.axvline(st_time, color='red', dashes=(5,3))

        self._stop_time_line.remove()
        self._stop_time_line = self._td_line.axes.axvline(et_time, color='red', dashes=(5,3))

        self._td_line.figure.canvas.draw()


        # plot the PSD
        self._fd_line.set_data(fd_bins/np.max(fd_bins),fd_sig)

        # add markers for the bandwidth in the PSD
        lhs_bw = 2*cf-bw
        rhs_bw = 2*cf+bw
        self._lhs_bw_line.remove()
        self._lhs_bw_line = self._fd_line.axes.axvline(lhs_bw, color='red', dashes=(5,3))

        self._rhs_bw_line.remove()
        self._rhs_bw_line = self._fd_line.axes.axvline(rhs_bw, color='red', dashes=(5,3))

        self._fd_line.figure.canvas.draw()

    def _update_sig_param_labels(self, snr, bw, cf, ss, es, doa):
        self.snr_out.setText(str(snr))
        self.bw_out.setText(str(bw))
        self.cf_out.setText(str(cf))
        self.st_out.setText(str(ss))
        self.et_out.setText(str(es))
        self.doa_out.setText(str(doa))

    
    def _update_feature_maps(self,in_sig):
        # grab the feature maps
        self.fms = np.squeeze(self.fm_model.predict(np.array([in_sig])))
        
        # normalize the feature maps
        f_max, f_min = self.fms.max(), self.fms.min()
        self.fms = (self.fms - f_min)/(f_max - f_min)

        # update the feature map displays
        self._update_displayed_fms(self.fm_slider.value())

    def _generate_kernals(self):
        # get the convolutional layers of interest
        cnn_layer = self.model.layers[1]
        #for i in range(2,10):
        #   cnn_layer = self.model.layers[i](cnn_layer)

        # seperate into the filters and bias
        filters, _ = cnn_layer.get_weights()

        # normalize the filters
        f_max, f_min = filters.max(), filters.min()
        filters = (filters - f_min)/(f_max - f_min)

        return filters

    def _update_displayed_kernals(self, value):
        # loop through and update each kernal plot based off the value given
        for row_idx in range(self.keranal_plot_rows):
            for col_idx in range(self.keranal_plot_cols):
                # get axis for specific kernal
                plot = self.kernal_plots[row_idx,col_idx]
                ax = self.kernal_axes[row_idx,col_idx]
                
                # plan for the below statement -> value*panel_per_plot + unique frame index
                kernal_idx = value*(self.keranal_plot_rows * self.keranal_plot_cols) + (row_idx+col_idx*self.keranal_plot_rows)
                # plot the kernal if haven't reached them all
                if kernal_idx < self.kernals.shape[-1]:
                    plot.set_data(self.kernals[:,:,kernal_idx])
                    ax.set_title('{}'.format(kernal_idx))
                
        
        plot.figure.canvas.draw()
        

    def _update_displayed_fms(self, value):
        # loop through and update each fm plot based off the value in fm slider bar
        for row_idx in range(self.fm_plot_rows):
            for col_idx in range(self.fm_plot_cols):
                # get axis for specific kernal
                plot = self.fm_plots[row_idx,col_idx]
                ax = self.fm_axes[row_idx,col_idx]

                fm_idx = value*(self.fm_plot_rows * self.fm_plot_cols) + (row_idx+col_idx*self.fm_plot_rows)
                # plot the kernal if haven't reached them all
                if fm_idx < self.fms.shape[-1]:
                    plot.set_data(self._fm_sample_space, self.fms[:,fm_idx])
                    ax.set_title('{}'.format(fm_idx))
      
        plot.figure.canvas.draw()



if __name__ == "__main__":
    # Check whether there is already a running QApplication (e.g., if running
    # from an IDE).
    qapp = QtWidgets.QApplication.instance()
    if not qapp:
        qapp = QtWidgets.QApplication(sys.argv)



    app = ApplicationWindow()
    app.show()
    app.activateWindow()
    app.raise_()
    qapp.exec()