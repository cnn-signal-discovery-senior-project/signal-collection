"""
    This script is ment for running data collection at a single datapoint using 
    SOUNDER. a datapoint is a single location in which singles are captured at 
    multiple differing configurations.

    For basic usage run the scrtip and  specify a Sounder configuration file that
    you want sounder to run with, alternativly yoou can specify --use-mult-configs=True, in which case 
    you will pass in the folder holding the configuration files for a data collection. the directory 
    holding the configuration files should follow the following structure

    -config_dir
        -configs
            -configA.json
            -configB.json
            ...
        -topology
            -topology.json
            ...
    Then you would specify the config_dir/configs as the parameter so that the recorder only 
    processes valid SOUNDER config files. Also if yoou are using a folder of configurations 
    then some optioal features such as automated gain changing will not work. This is a deliberate
    choice as it will increase run time a lot to run multiple configurations at multiple gain settings

    This script will only consider the configuration files in the configs directory and 
    run them sequentially.

    other parameters include the following.

    --sounder-loc -> specifyes the location of the sounder exevutable
    --sounder-out -> specifies the location where you want SOUNDER hdf5 files to be stored
    --use-mult-gains -> run the applicatioon multiple times using different configurations
    --num-gains -> if using use_mult_gains then this specifies the number of random gains to use


Joshuamiraglia@gmail.com
"""
import os
import subprocess
from subprocess import PIPE
from optparse import OptionParser
import time
import json
from matplotlib.pyplot import pause
import numpy.random as rand
import glob

def run_sounder(sounder_loc, conf_path, storepath):
    com = [sounder_loc, "-conf", conf_path, "-storepath", storepath]
    p = subprocess.Popen(com)
    #r = subprocess.call(com)
    p.wait()

    r = 0 # currently Sounder always return 0 on exit so can't tell if it fails or not, so do woork later too fix this
    #r = p.returncode
    print('SOUNDER process returned with code {}'.format(r))
    if r != 0:
        # if it doens't successed it could be because of bad netwrok conenction to a radio which can ususally fix itself if you run
        # the application again so try that here
        print("unsuccefully ran SOUNDER using config file {}, trying one more time".format(conf_path))
        
        com = [sounder_loc, "--conf", conf_path, "--storepath", storepath]
        #p = subprocess.Popen(com, stdout=PIPE, stderr=PIPE)
        #p.wait()

        #r = p.returncode
        r = subprocess.call(com)
        if r != 0: 
            print("unsuccefully ran SOUNDER for config file {} for the second time now moving on".format(conf_path))
            return 0

    return 1

def main():
    ###############################################
    # Parse optional parameters
    ##################################################
    parser = OptionParser()
    parser.add_option("--conf", type="string", dest="conf", help="Directory loook for configuration files to run through", default="./config/sounder_conf/configs")
    parser.add_option("--use-mult-configs", dest="use_mult_configs", help="Specifies if using multiple configuration file, in which case use conf as a directory rather than file. also if multiple coonfiguration are used multiple gains cannot be used", default=False)
    parser.add_option("--sounder-loc", type="string", dest="sounder_loc", help="Location of SOUNDER executable", default="/scratch/RENEWLab/CC/Sounder/build/sounder")
    parser.add_option("--sounder-out", type="string", dest="sounder_out", help="specifies the location where you want SOUNDER hdf5 files to be stored", default="./")
    parser.add_option("--use-pregen-data", dest="use_pregen_data", help="specifies if you want to use pre generated binary files for SOUNDER of if you want to generate them", default=True)
    parser.add_option("--pregen-data-dir", type="string", dest="pregen_data_dir", help="specifies the location for pregenerated data,  i.e. the direcotry of a predefined SOUNDER storepath", default="./")
    parser.add_option("--use-mult-gains", dest="use_mult_gains", help="run the applicatioon multiple times using different configurations", default=True)
    parser.add_option("--num-gains", type='int', dest="num_gains", help="if using use_mult_gains then this specifies the number of random gains to use", default=3)

    (options, args) = parser.parse_args()

    conf = options.conf
    use_mult_configs = options.use_mult_configs
    sounder_loc = options.sounder_loc
    sounder_out = options.sounder_out
    use_pregen_data = options.use_pregen_data
    pregen_data_dir = options.pregen_data_dir
    use_mult_gains = options.use_mult_gains
    num_gains = options.num_gains

    if not os.path.isdir(sounder_out):
            print("output directory {} does not exist will try creating it".format(sounder_out))
            p = subprocess.Popen(['mkdir', sounder_out], stdout=PIPE, stderr=PIPE)
            p.wait()

            r = p.returncode
            if r != 0:
                print('could not make directory, exiting...')
                return

    if not use_pregen_data:
        print("generting data is not yet supported, exiting")
        return

    sounder_storepath = pregen_data_dir # create a temp dir if usingpregen data later on

    if use_mult_configs:
        print('running all configs in directory {}'.format(conf))
        if not os.path.isdir(conf):
            print("configuration directory {} does not exist exiting..".format(conf))
            return

        # get all configuration files to process 
        conf_files = os.listdir(conf)
        for file in conf_files:
            # quick check to only use json config files
            if not file.endswith(".json"):
                continue
            
            # run sounder using the current configuration file
            conf_path = os.path.join(conf, file)
            print('running SOUNDER for configutation file {}'.format(conf_path))
            
            run_sounder(sounder_loc, conf_path, sounder_storepath)
            


    else:
        # check to see if file exists
        if not os.path.isfile(conf):
            print("configuration file {} does not exist exiting..".format(conf))
            return

        # do some processing on the configuration if using multiple gains
        if use_mult_gains:
            # load in json for the config file
            fin = open(conf, 'r')
            var_conf = json.load(fin)

            # specify gain range to use and iterate the application for the amount of random gains
            # to generate
            low_gain = 40
            high_gain = 81
            prev_gain = 0 # initial at 0 so it wont cause problems
            gain_tolerance = 15 # the amount of difference the gain needs to have from previos one
            for i in range(num_gains):
                # generate teh random gain and make sure it isn't too close to the previous gain
                gain = rand.randint(low_gain,high_gain)
                while abs(prev_gain-gain) < gain_tolerance:
                    gain = rand.randint(low_gain,high_gain)

                # change the field for client tx gain in the configuration file and save it 
                # temporarily in the storepath while SOUNDER uses it
                var_conf['Clients']['txgainA'] = [gain, gain]
                var_conf['Clients']['txgainB'] = [gain, gain]

                config_path = os.path.join(sounder_storepath, "config.json")
                with open(config_path, "w") as outfile:
                    json.dump(var_conf,outfile)

                # run the sounder application using the modified config
                print('Runnning SOUNDER with gain: {}'.format(gain))
                run_sounder(sounder_loc,config_path,sounder_storepath)

                # delete the configuration file
                subprocess.call(['rm', config_path]) 

                prev_gain = gain 

            #time.sleep(5)

        else:
            run_sounder(sounder_loc, conf, sounder_storepath)

        # move the returned file to the user specified location
        files = glob.glob(os.path.join(sounder_storepath,"trace*"))
        print(files)
        print(os.path.join(sounder_storepath,"trace*"))
        for file in files:
            com = ['mv', file, sounder_out]
            print(com)
            subprocess.call(com)

            



if __name__ == '__main__':
    scrpt_strt = time.time()
    main()
    scrpt_end = time.time()
    print(">>>> Script Duration: time: %f \n" % ( scrpt_end - scrpt_strt) )