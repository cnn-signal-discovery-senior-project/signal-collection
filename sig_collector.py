"""
    This script is for runnign a single cycle of data colelction with the mimo array and client radio 
    remaining in a static position. this script will then run multiple signals from the client to the mimo array
    and collect the recieved signals along with usful signal parameters as labels into the .h5 file format,
    which is defined here https://portal.hdfgroup.org/display/HDF5/HDF5.

"""
import os
import yaml
import h5py
import numpy as np
import matplotlib.pyplot as plt
import time


import utils.sig_gen as sig_gen
import utils.signal_processing as sig_proc
import faulthandler
from radios.signal_sender import signalSender

def main():
    # get relevant information from the experiment configuration file
    base_dir = os.path.dirname(os.path.abspath(__file__)) # directory of the experiment
    with open(os.path.join(base_dir, 'config','experiment_config.yaml')) as config_file:
        try:
            exp_param = yaml.safe_load(config_file)
        except yaml.YAMLError as exc:
            print(exc)
            print('closing') # possibly instead of closing include a list of default values
            return

        use_pregen_data = exp_param['pregen_data']
        debug = exp_param['pregen_data']


        # open the file for saving the signal data
        fout = os.path.join(base_dir,'test_data.h5')
        with h5py.File(fout, 'w') as data_out:
            # instaciate the hdf5 databases
            sig_len = exp_param['signal_length']
            num_sigs = exp_param['num_signals']
            out_sig = data_out.create_dataset('signals', (num_sigs,sig_len),dtype=complex)

            labels = data_out.create_group('labels')
            out_freq = labels.create_dataset('frequency',(2,num_sigs), dtype=int)
            #labels.create_dataset('time', (0,))
            out_sig_prop = labels.create_dataset('general', (2,num_sigs), dtype=float)

            metadata = data_out.create_group('metadata')
            out_spatial = metadata.create_dataset('spatial_data', (2,3), dtype=float)
            out_angle = metadata.create_dataset('angle', (1,), dtype=float)

            # write in metadata values as they don't change from signal to signal
            out_spatial[0,:] = exp_param['mimo_coords']
            out_spatial[1,:] = exp_param['client_coords']

            out_angle[0] = exp_param['mimo_angle']


            # now we run the loop of signal gernation, signal tx/rx, and collection
            # initialize the signal sender class

            sig_sender = si(bsnodes=exp_param['bsnode'], clnode=exp_param['clnode'], numSamps=sig_len)

            if debug:
                fig = plt.figure(figsize=(24, 8), dpi=120)
                fig.subplots_adjust(hspace=.5, top=.85)

            # Select where to grab signals from for sending and collection
            get_signals = sig_gen.sig_gen_from_file
            if not use_pregen_data:
                print('currently not up to date, caution if you use this method')
                import utils.sig_gen_realtime as sig_gen_realtime
                #TODO: update realtime signal generation to use the better signal generator
                get_signals = sig_gen_realtime.gen_signals

            for i, sig_data in enumerate((get_signals(exp_param))):
                tx_sig = sig_data[0]
                cen_freq = sig_data[1]
                bandwidth = sig_data[2]
                amp = sig_data[3]

                # send the signal the mimo setup
                rx_sig_A, rx_sig_B = sig_sender.send_recieve_sig(tx_sig)
                #rx_sig_A = sig_proc.sim_sig_txrx(tx_sig)

                # record the recieved signal along with relavent information about the signal and the experiment setup
                out_freq[0,i] = cen_freq
                out_freq[1,i] = bandwidth

                out_sig_prop[0,i] = amp

                out_sig[i,:] = rx_sig_A

                if debug:
                    ax1 = fig.add_subplot(2, 2, 1)
                    ax2 = fig.add_subplot(2, 2, 2)
                    ax3 = fig.add_subplot(2, 2, 3)
                    ax4 = fig.add_subplot(2, 2, 4)
                    print('plotting')

                    ax1.grid(True)
                    ax1.set_ylabel('Signal (units)')
                    ax1.set_xlabel('Sample index')
                    ax1.title.set_text('scatter plot of tx signal')
                    #ax1.plot(range(len(tx_sig)), np.real((tx_sig)), label='ChA real cl')
                    ax1.plot(range(len(tx_sig)), np.abs(tx_sig), label='ChA imag cl')
                    #ax1.scatter(np.real((tx_sig)), np.imag((tx_sig)))
                    ax1.set_ylabel('imag')
                    ax1.set_xlabel('real')
                    #ax1.set_ylim(-1, 1)
                    ax1.legend(fontsize=10)

                    tx_freq, tx_powerBins, nf, peaks = sig_proc.fft_power(rx_sig_A, 5e6)
                    ax2.grid(True)
                    ax2.title.set_text('PSD of tx signal')
                    ax2.set_ylabel('Signal (dB)')
                    ax2.set_xlabel('frequency [Hz]')
                    ax2.plot(tx_freq, tx_powerBins)
                    ax2.legend(fontsize=10)

                    ax3.grid(True)
                    ax3.title.set_text('scatter plot of rx signal')
                    ax3.set_ylabel('Signal (units)')
                    ax3.set_xlabel('Sample index')
                    #ax3.plot(range(len(rx_sig_A)), np.real((rx_sig_A)), label='ChA Real rx')
                    #ax3.plot(range(len(rx_sig_A)), np.abs((rx_sig_A)), label='ChA Imag rx')
                    ax3.scatter(np.real(rx_sig_A), np.imag(rx_sig_A))
                    ax3.set_ylabel('imag')
                    ax3.set_xlabel('real')
                    ax3.legend(fontsize=10)
                    #ax2.set_ylim(-1, 1)

                    rx_freq, powerBins, nf, peaks = sig_proc.fft_power(rx_sig_A, 5e6)
                    print(bandwidth)
                    ax4.grid(True)
                    ax4.title.set_text('PSD of rx signal')
                    ax4.set_ylabel('Signal (dB)')
                    ax4.set_xlabel('frequency [Hz]')
                    ax4.plot(rx_freq, powerBins)
                    ax4.legend(fontsize=10)
                
                    plt.show()

        


if __name__ == '__main__':
    ts = time.time()
    main()
    tf = time.time()
    print('elapsed time: %f' % ((float(tf-ts)*float(1e-9)),) )