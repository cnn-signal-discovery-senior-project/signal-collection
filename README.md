## Project Overview

This project contains scripts for collecting and generating data for the signal discovery project. The prject current send and capture signals in 2 different ways. First is a signal stream application for sending from a single client to be recieved by a single element of the MMIMO array. The second, is a modified version of the SOUNDER application created by RENEWLabs which sends from 2 client radios and collected by all elements in the MMIMO array. Both data collection workflows work using the below dataflow diagram and use of both is further described below.

![Signal collector process](res/py_sig_collector_flowchart.png)

### Single Stream collector
The application is written entirly in python and the main loop of the data collector is in the script sig_collector.py. To run the application you edit the experiment_config.yaml in the config folder to specify the cient and basestation antennas to use, If you don't know the ids of the radios run the command python3 -m pyfaros.discover . The single stream collector can also be ran using pregenerated data files following the format specified in the Signal Generation section, or use the realtime generation of signals by setting the pre_gen_data parameter in the configuration file. Other usful parameters to specify in the are listed below

debug -> plot the first sent signal for analysis





### SOUNDER
This data collection script is for collecting large amounts of data from multiple MMIMO elements from one or more clients at different congiurations. To do this we use a modified SOUNDER application found [here](https://gitlab.flux.utah.edu/cnn-signal-discovery-senior-project/sounder). 

#### **Getting Started**
The configuration files for running this script are found in the.

![Signal collector process](res/sig_collector_diagram.png)

## Signal Generation
 Currently our signal generation is done in 2 ways, realtime signal generation and file signal generation. primarily for signal collection file signal generation is used where the signals are pre-made with gnuradio and then loaded in to the signal generation script to be sent from the client radio to the MIMO reciever. these files are saved into a hdf5 file format outlined below

 // put an image here for the signal generation data format

## Collected data format

Use the hdf5 data format for this project for its ability to handle large amounts of object-centric data, a good overview of the format can be found [here:](https://www.neonscience.org/resources/learning-hub/tutorials/about-hdf5#:~:text=The%20Hierarchical%20Data%20Format%20version,with%20files%20on%20your%20computer.)

For our purposes we use the hdf5 arcitecture below for a given session. Parameters such as radio and MIMO positions and angles are recorded only once per session, so if you want to combine multiple sessions into a single data file you will need to make these parameters be saved per signal rather than per session.

![Data Format](res/sig_collector_data_diagram.png)

