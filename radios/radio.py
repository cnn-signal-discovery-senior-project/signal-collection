#!/usr/bin/python3
"""
  This class is reposndible for handlign the setup and sending of retrieving of signles
  between a single client and a basestation. The communication is always transmittied from the 
  device designated as the client to hte base station.

  IMPORTANT NOTE:
  The client firmware has different features than the base station node
  firmware. Therefore, ONLY bsnodes can transmit a beacon whereas ONLY
  cnode can correlate against such beacon. This means it is critical to
  set bsnodes to the serial number of a base station node and clnode to the 
  serial number of a client node!
  NOTE ON GAINS:
  Gain settings will vary depending on RF frontend board being used
  If using CBRS:
  rxgain: at 2.5GHz [3:1:105], at 3.6GHz [3:1:102]
  txgain: at 2.5GHz [16:1:81], at 3.6GHz [15:1:81]
  If using only Dev Board:
  rxgain: at both frequency bands [0:1:30]
  txgain: at both frequency bands [0:1:42]
  The code assumes both TX and RX have the same type of RF frontend board.
  Also, currently AGC only supports the CBRS RF frontend. It cannot be used
  with the Iris Dev Board or UHF board
---------------------------------------------------------------------
 JOSHUA MIRAGLIA
 RENEW OPEN SOURCE LICENSE: http://renew-wireless.org/license
---------------------------------------------------------------------
"""

import sys
import os
sys.path.append(sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'utils', 'IrisUtils')))
sys.path.append(sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'utils')))

import random as rd
import threading
import SoapySDR
from SoapySDR import *  # SOAPY_SDR_ constants
import numpy as np
import time
import os
import signal
import math
import pdb
import json
import pickle
import scipy.io as sio 
from functools import partial
from type_conv import *
from file_rdwr import *
from generate_sequence import *
from signal_processing import cap_signal_frame

class radio():

    def __init__(self, bsnodes, rate=5e6, freq=3.6e9, txgain=80.0, rxgain=70.0, numSamps=512, numSyms=10, txSymNum=1, threshold=1,
                 prefix_length=100, postfix_length=100, both_channels=False, calibrate=False, agc_en=False, record=True):
        # Save the setup info for the class
        self.rate = rate
        self.freq = freq
        self.txgain = txgain
        self.rxgain = rxgain
        self.numSamps = numSamps # the number of samples to recieve in a given transmission
        self.prefix_length = prefix_length
        self.postfix_length = postfix_length
        self.both_channels = both_channels # wether we are using two channels or one
        self.calibrate = calibrate
        self.record = record
        self.symSamp = numSamps + prefix_length + postfix_length # the amount of samples per symbol in the TDD frame schedule

        # initialize the settings for the hardware
        print("setting %s as BS" % (bsnodes))
        self.bsdr = []
        print(bsnodes)
        for bs in bsnodes:
            self.bsdr.append(SoapySDR.Device(dict(serial=bs, driver="iris")))

        #############################################
        # Some default sample rates
        #############################################
        for i, sdr in enumerate(self.bsdr):
            info = sdr.getHardwareInfo()
            print("%s settings on device %d" % (info["frontend"], i))
            for ch in [0, 1]:
                sdr.setBandwidth(SOAPY_SDR_TX, ch, 2.5*rate)
                sdr.setBandwidth(SOAPY_SDR_RX, ch, 2.5*rate)
                sdr.setSampleRate(SOAPY_SDR_TX, ch, rate)
                sdr.setSampleRate(SOAPY_SDR_RX, ch, rate)
                #sdr.setFrequency(SOAPY_SDR_TX, ch, freq)
                #sdr.setFrequency(SOAPY_SDR_RX, ch, freq)
                sdr.setFrequency(SOAPY_SDR_TX, ch, 'RF', freq-.75*rate)
                sdr.setFrequency(SOAPY_SDR_RX, ch, 'RF', freq-.75*rate)
                sdr.setFrequency(SOAPY_SDR_TX, ch, 'BB', .75*rate)
                sdr.setFrequency(SOAPY_SDR_RX, ch, 'BB', .75*rate)
                sdr.setAntenna(SOAPY_SDR_RX, ch, "TRX")
                sdr.setDCOffsetMode(SOAPY_SDR_RX, ch, True)

                if "CBRS" in info["frontend"]:
                    # Set gains to high val (initially)
                    if agc_en: rxgain = 100
                    sdr.setGain(SOAPY_SDR_TX, ch, txgain)
                    sdr.setGain(SOAPY_SDR_RX, ch, rxgain)
                else:
                    # No CBRS board gains, only changing LMS7 gains
                    # AGC only supported for CBRS boards
                    agc_en = False
                    sdr.setGain(SOAPY_SDR_TX, ch, "PAD", txgain)    # [0:1:42]
                    sdr.setGain(SOAPY_SDR_TX, ch, "IAMP", 0)        # [-12:1:3]
                    sdr.setGain(SOAPY_SDR_RX, ch, "LNA", rxgain)    # [0:1:30]
                    sdr.setGain(SOAPY_SDR_RX, ch, "TIA", 0)         # [0, 3, 9, 12]
                    sdr.setGain(SOAPY_SDR_RX, ch, "PGA", -10)       # [-12:1:19]

                # Read initial gain settings
                readLNA = sdr.getGain(SOAPY_SDR_RX, 0, 'LNA')
                readTIA = sdr.getGain(SOAPY_SDR_RX, 0, 'TIA')
                readPGA = sdr.getGain(SOAPY_SDR_RX, 0, 'PGA')
                print("INITIAL GAIN - LNA: {}, \t TIA:{}, \t PGA:{}".format(readLNA, readTIA, readPGA))

            for ch in [0, 1]:
                if calibrate:
                    sdr.writeSetting(SOAPY_SDR_RX, ch, "CALIBRATE", 'SKLK')

            sdr.writeSetting("RESET_DATA_LOGIC", "")

        # Initialize streams for recieving and transmitting
        self.rxStreamBS = []
        for bsdr in self.bsdr:
            self.rxStreamBS.append(bsdr.setupStream(SOAPY_SDR_RX, SOAPY_SDR_CF32, [0, 1]))

        # Adjust settings for amplifier control
        agc_conf = {"agc_enabled" : agc_en}
        for bsdr in self.bsdr:
            bsdr.writeSetting("AGC_CONFIG", json.dumps(agc_conf))


        
    """
        This function will send a signal from the client device to the base array 
        and record a single sample of the full signal
        INPUTS:
            signal (numpy.array): the signal to transmit to the base array should be of length numSamp
        RETURN:
            the recieved signal at the base array
    """
    def send_recieve_sig(self, txSignal):
        if (len(txSignal) != self.numSamps):
            print("signal was size %d, should be of size %d" % (len(txSignal),self.numSamps))
            return -1

        ######################################################
        # Setting up client transmission
        ######################################################
        #activate streams used on the client
        self.clsdr.activateStream(self.txStreamCL)
        self.clsdr.activateStream(self.rxStreamCL, 0, 0)

        # prep the signal for both channels, loading all 0's into channel B
        preamble_size = 50
        txpilot = np.concatenate((np.array([0]*preamble_size, dtype=np.complex64),txSignal)) # add a small preamble to the signle so we can tell when the signal starts later on
        pilot_ui32 = cfloat2uint32(txpilot, order='QI')
        wbz = np.array([0]*self.numSamps, np.uint32)

        # get the current timestamp from the client radio
        offset = 50
        txTime = 0
        waveRxA = np.array([0]*self.numSamps, np.complex64) # empty arrays for the readstream function
        waveRxB = np.array([0]*self.numSamps, np.complex64)
        sr = self.clsdr.readStream(self.rxStreamCL, [waveRxA, waveRxB], self.numSamps)
        if sr.ret > 0:
            txTime = sr.timeNs & 0xFFFFFFFF00000000
            txTime += (0x000000000 + (offset << 16)) # shift the transmission time allow time for the recieving buffer to be setup

        else:
            # exit thread if not able to sync up times
            print('problem reading stream, ret: %d' % sr.ret)
            return
        
        # possibly set the hardwardtime of the base station to reflect the client
        # or just get the time for both devices at the same time and just syncrinize the offsets
        # or just set both clock to th same value and hope for low offset

        # stream the transmission signal at the appropriate time calculate above
        flags = SOAPY_SDR_HAS_TIME
        #st = self.clsdr.writeStream(self.txStreamCL, [txSignal, wbz], self.numSamps, flags, timeNs=txTime)
        replay_addr = 0
        self.clsdr.writeRegisters("TX_RAM_A", replay_addr, pilot_ui32.tolist())
        self.clsdr.writeRegisters("TX_RAM_B", replay_addr, wbz.tolist())
        self.clsdr.writeSetting("TX_REPLAY", str(self.numSamps+preamble_size))

        ######################################################
        # recieve the signal from the base station
        ########################################################
        waveRxA = []
        waveRxB = []
        for bsdr in self.bsdr:
            waveRxA.append(np.array([0]*self.numSamps, np.complex64))
            waveRxB.append(np.array([0]*self.numSamps, np.complex64))
        
        waveRxA = np.zeros((len(self.bsdr), self.numSamps), dtype=np.complex64) # empty arrays for the readstream function
        waveRxB = np.zeros((len(self.bsdr), self.numSamps), dtype=np.complex64)

        # activate the rx stream for the base station
        rxOffset = -8 # rx offset for when to stsrt reading based off time of flight and other delays
        rxTime = sr.timeNs & 0xFFFFFFFF00000000
        rxTime += (0x000000000 + ((offset+rxOffset) << 16)) # shift the transmission time allow time for the recieving buffer to be setup

        flags = SOAPY_SDR_ONE_PACKET | SOAPY_SDR_END_BURST
        for i, bsdr in enumerate(self.bsdr):
            r1 = bsdr.activateStream(self.rxStreamBS[i], flags, 0, self.numSamps)
            if r1<0:
                print("Problem activating stream #1 for bs %d" % i)
        

        # Read the transmission signals from the client
        for i, bsdr in enumerate(self.bsdr):
            sr = bsdr.readStream(self.rxStreamBS[i], [waveRxA[i,:], waveRxB[i,:]], self.numSamps*2)
            if sr.ret < 0 or sr.ret > self.numSamps:
                print("BS: readStream returned %d"%sr.ret)
            
        # do some post processing of the signle to crop the rx sig at the preamble to account for phase shifting of the signal
        #waveRxA[0,:self.numSamps] = cap_signal_frame(waveRxA[0,:], self.numSamps)
        #waveRxA[0,self.numSamps:] = 0

        # deactivate the base station rx stream
        for i, bsdr in enumerate(self.bsdr):
            bsdr.deactivateStream(self.rxStreamBS[i])

        return waveRxA, waveRxB

    def tx(self, txSignal, stream=True):
        if (len(txSignal) != self.numSamps):
            print("signal was size %d, should be of size %d" % (len(txSignal),self.numSamps))
            return -1

        ######################################################
        # Setting up radio transmission
        ######################################################
        #activate streams used on the client
        self.clsdr.activateStream(self.txStreamCL)

        # prep the signal for both channels, loading all 0's into channel B
        preamble_size = 50
        txpilot = np.concatenate((np.array([0]*preamble_size, dtype=np.complex64),txSignal)) # add a small preamble to the signle so we can tell when the signal starts later on
        pilot_ui32 = cfloat2uint32(txpilot, order='QI')
        wbz = np.array([0]*self.numSamps, np.uint32)
        
        # possibly set the hardwardtime of the base station to reflect the client
        # or just get the time for both devices at the same time and just syncrinize the offsets
        # or just set both clock to th same value and hope for low offset

        # stream the transmission signal at the appropriate time calculate above
        #st = self.clsdr.writeStream(self.txStreamCL, [txSignal, wbz], self.numSamps, flags, timeNs=txTime)
        if stream:
            replay_addr = 0
            self.clsdr.writeRegisters("TX_RAM_A", replay_addr, pilot_ui32.tolist())
            self.clsdr.writeRegisters("TX_RAM_B", replay_addr, wbz.tolist())
            self.clsdr.writeSetting("TX_REPLAY", str(self.numSamps+preamble_size))

        

    def rx(self):
        ######################################################
        # recieve the signal from the base station
        ########################################################
        waveRxA = []
        waveRxB = []
        for bsdr in self.bsdr:
            waveRxA.append(np.array([0]*self.numSamps, np.complex64))
            waveRxB.append(np.array([0]*self.numSamps, np.complex64))
        
        waveRxA = np.zeros((len(self.bsdr), self.numSamps), dtype=np.complex64) # empty arrays for the readstream function
        waveRxB = np.zeros((len(self.bsdr), self.numSamps), dtype=np.complex64)

        # activate the rx stream for the base station
        flags = SOAPY_SDR_ONE_PACKET
        for i, bsdr in enumerate(self.bsdr):
            r1 = bsdr.activateStream(self.rxStreamBS[i], flags, 0, self.numSamps)
            if r1<0:
                print("Problem activating stream #1 for bs %d" % i)
        

        # Read the transmission signals from the client
        for i, bsdr in enumerate(self.bsdr):
            sr = bsdr.readStream(self.rxStreamBS[i], [waveRxA[i,:], waveRxB[i,:]], self.numSamps*2)
            if sr.ret < 0 or sr.ret > self.numSamps:
                print("BS: readStream returned %d"%sr.ret)

        # deactivate the base station rx stream
        for i, bsdr in enumerate(self.bsdr):
            bsdr.deactivateStream(self.rxStreamBS[i])

        return waveRxA, waveRxB

    def set_gain(self, rx_gain, tx_gain):
        if rx_gain > 80 or tx_gain > 80:
            print("gains can't be set above 81")
            return 0

        for i, sdr in enumerate(self.bsdr):
            info = sdr.getHardwareInfo()
            for ch in [0, 1]:
                if "CBRS" in info["frontend"]:
                    sdr.setGain(SOAPY_SDR_TX, ch, tx_gain)
                    sdr.setGain(SOAPY_SDR_RX, ch, rx_gain)
                else:
                    # No CBRS board gains, only changing LMS7 gains
                    # AGC only supported for CBRS boards
                    agc_en = False
                    sdr.setGain(SOAPY_SDR_TX, ch, "PAD", tx_gain)    # [0:1:42]
                    sdr.setGain(SOAPY_SDR_TX, ch, "IAMP", 0)        # [-12:1:3]
                    sdr.setGain(SOAPY_SDR_RX, ch, "LNA", rx_gain)    # [0:1:30]
                    sdr.setGain(SOAPY_SDR_RX, ch, "TIA", 0)         # [0, 3, 9, 12]
                    sdr.setGain(SOAPY_SDR_RX, ch, "PGA", -10)       # [-12:1:19]
        return 1
        

    def __del__(self):
        # clean up the sdr deivces for both the base station and the client
        # close streams
        for i,bsdr in enumerate(self.bsdr):
            bsdr.closeStream(self.rxStreamBS[i])

        # ADC_rst, stops the tdd time counters
        tdd_conf = {"tdd_enabled" : False}
        for sdr in self.bsdr:
            sdr.writeSetting("RESET_DATA_LOGIC", "")
            sdr.writeSetting("TDD_CONFIG", json.dumps(tdd_conf))
            sdr.writeSetting("TDD_MODE", "false")

        self.bsdr = None
