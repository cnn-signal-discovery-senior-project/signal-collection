#!/usr/bin/python3
"""
  This class is reposndible for handlign the setup and sending of retrieving of signles
  between a single client and a basestation. The communication is always transmittied from the 
  device designated as the client to hte base station.

-----------------------------
 JOSHUA MIRAGLIA
 RENEW OPEN SOURCE LICENSE: http://renew-wireless.org/license
---------------------------------------------------------------------
"""

import sys
import os
sys.path.append(sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'utils')))

import random as rd

import numpy as np
from sig_gen import rand_sig_from_file

class Simradio():

    def __init__(self, rate=5e6, freq=3.6e9, txgain=80.0, rxgain=70.0, numSamps=512,
                 prefix_length=0, postfix_length=0, both_channels=False, sig_file="F:\POWDER\gen_data\Anechoic_Data.h5"):
        # Save the setup info for the class
        self.rate = rate
        self.freq = freq
        self.txgain = txgain
        self.rxgain = rxgain
        self.numSamps = numSamps # the number of samples to recieve in a given transmission
        self.prefix_length = prefix_length
        self.postfix_length = postfix_length
        self.both_channels = both_channels # wether we are using two channels or one

        self.symSamp = numSamps + prefix_length + postfix_length # the amount of samples per symbol in the TDD frame schedule

        self.sig_file = sig_file

        
    """
        This function will send a signal from the client device to the base array 
        and record a single sample of the full signal
        INPUTS:
            signal (numpy.array): the signal to transmit to the base array should be of length numSamp
        RETURN:
            the recieved signal at the base array
    """
    def send_recieve_sig(self, txSignal):
        signal_size = len(txSignal)

        # simulated a phase shift of the signal for the rx reciever and capture 2 copies of tx sig
        phase_ind = np.random.randint(25,signal_size-25)
        rx_sig = np.array([0]*signal_size, np.complex)

        rx_sig[0:signal_size-phase_ind] = txSignal[phase_ind:]
        rx_sig[signal_size-phase_ind:signal_size] = txSignal[:phase_ind]
        
        #rx_sig = np.array([0]*signal_size*2, np.complex)

        #rx_sig[0:signal_size-phase_ind] = tx_sig[phase_ind:]
        #rx_sig[signal_size-phase_ind:(2*signal_size)-phase_ind] = tx_sig
        #rx_sig[(2*signal_size)-phase_ind:] = tx_sig[:phase_ind]
        
        # add noise
        rx_sig += np.random.normal(0,.05, size=(len(rx_sig),2)).view(np.complex)[:,0]

        return rx_sig

    """
    perform the rx function for the radio, here we generate a random signal and return that
    
    """
    def rx(self):
        return rand_sig_from_file(self.sig_file)


    def set_gain(self, rx_gain, tx_gain):
        # TODO: add gain simulation to the radio
        return
        

