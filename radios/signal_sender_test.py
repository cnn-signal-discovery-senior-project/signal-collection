import sys
import os
sys.path.append(sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'utils', 'IrisUtils')))
sys.path.append(sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'utils')))
sys.path.append(sys.path.append(os.path.join(os.path.dirname(__file__), '..')))


from signal_sender import signalSender
from sig_gen import gen_sine_sig
import matplotlib.pyplot as plt
from optparse import OptionParser
import time
import numpy as np
from fft_power import *




#########################################
#                  Main                 #
#########################################
def main():
    parser = OptionParser()
    parser.add_option("--bsnode", type="string", dest="bsnode", help="serial number of the master (base station node) device", default=["RF3E000256","RF3E000387"])
    parser.add_option("--clnode", type="string", dest="clnode", help="serial number of the slave (client node) device", default="RF3E000157")
    parser.add_option("--hub", type="string", dest="hub", help="Hub node", default="")
    parser.add_option("--rate", type="float", dest="rate", help="Tx sample rate", default=5e6)
    parser.add_option("--txgain", type="float", dest="txgain", help="Tx gain (dB)", default=80.0)  # Check top of file for info on gain range
    parser.add_option("--rxgain", type="float", dest="rxgain", help="Rx gain (dB)", default=70.0)  # Check top of file for info on gain range
    parser.add_option("--freq", type="float", dest="freq", help="Optional Tx freq (Hz)", default=3.6e9)
    parser.add_option("--numSamps", type="int", dest="numSamps", help="Num samples to receive", default=512)
    parser.add_option("--prefix-length", type="int", dest="prefix_length", help="prefix padding length for beacon and pilot", default=100)     # to compensate for front-end group delay
    parser.add_option("--postfix-length", type="int", dest="postfix_length", help="postfix padding length for beacon and pilot", default=100)  # to compensate for rf path delay
    parser.add_option("--numSyms", type="int", dest="numSyms", help="Number of symbols in one sub-frame", default=20)
    parser.add_option("--txSymNum", type="int", dest="txSymNum", help="Number of tx sub-frames in one frame", default=0)
    parser.add_option("--corr-threshold", type="int", dest="threshold", help="Correlator Threshold Value", default=1)
    parser.add_option("--ue-tx-advance", type="int", dest="tx_advance", help="sample advance for tx vs rx", default=68)
    parser.add_option("--both-channels", action="store_true", dest="both_channels", help="transmit from both channels", default=False)
    parser.add_option("--calibrate", action="store_true", dest="calibrate", help="transmit from both channels", default=False)
    parser.add_option("--use-trig", action="store_true", dest="use_trig", help="uses chain triggers for synchronization", default=False)
    parser.add_option("--wait-trigger", action="store_true", dest="wait_trigger", help="wait for a trigger to start a frame", default=False)
    parser.add_option("--tx-power-loop", action="store_true", dest="tx_power_loop", help="loop over a set of tx gains in consecutive frames", default=False)
    parser.add_option("--record", action="store_true", dest="record", help="record received pilots and data", default=True)
    parser.add_option("--agc-enable", action="store_true", dest="agc_en", help="Enable AGC flag", default=False)
    (options, args) = parser.parse_args()

    if options.txgain > 81:
        print("[ERROR] TX gain should be between 0 and 81")
        exit(0)

    bsnodes = ['RF3E000256']
    #bsnodes = options.bsnode
    sig_sender = signalSender(bsnodes=bsnodes,
    #                             clnode=options.clnode)
    #sig_sender = TDDradio(bsnode=options.bsnode,
                                 clnode=options.clnode)
    print('initializing radios...')
    time.sleep(2)
    print('done initializing')

    freqs = [20e3,1e6] # arrays of frequencies to plot
    rx_gains = [55,55,55,55,55,55]
    tx_gains = [25,40,50,60,70,81]

    #run through all frequencies for testing
    fig = plt.figure(figsize=(24, 8), dpi=120)
    fig.subplots_adjust(hspace=.5, top=.85)
    for rxgain, txgain in zip(rx_gains,tx_gains):
        if not sig_sender.set_gain(rxgain, txgain): continue # set the gains for teh next run of signals
        for n,freq in enumerate(freqs):
            # create a sample signal
            data = gen_sine_sig(freq, 1, sig_sender.rate, sig_sender.numSamps)

            rec_sig_A, rec_sig_B = sig_sender.send_recieve_sig(data)
            """
            rec_sig_A, rec_sig_B = tmpTDD.siso_sounder(
                                                    hub=options.hub,
                                                    serial1=options.bsnode,
                                                    serial2=options.clnode,
                                                    rate=options.rate,
                                                    freq=options.freq,
                                                    txgain=options.txgain,
                                                    rxgain=options.rxgain,
                                                    numSamps=sig_sender.numSamps,
                                                    numSyms=sig_sender.numSyms,
                                                    txSymNum=sig_sender.txSymNum,
                                                    threshold=options.threshold,
                                                    tx_advance=options.tx_advance,
                                                    prefix_length=options.prefix_length,
                                                    postfix_length=options.postfix_length,
                                                    both_channels=options.both_channels,
                                                    wait_trigger=options.wait_trigger,
                                                    calibrate=options.calibrate,
                                                    record=options.record,
                                                    tx_power_loop=options.tx_power_loop,
                                                    use_trig=options.use_trig,
                                                    agc_en=options.agc_en,
                                                )
            """

            #process and plot the signals
            ax1 = fig.add_subplot(len(freqs), 3, (3*n)+1)
            ax2 = fig.add_subplot(len(freqs), 3, (3*n)+2)
            ax3 = fig.add_subplot(len(freqs), 3, (3*n)+3)
            print('plotting')
            for i in range(len(bsnodes)):
                f1, powerBins, nf, peaks = fft_power(rec_sig_A[i,:], sig_sender.rate)

                # plot the signal
                ax1.set_title('tx signal  @ {}Hz'.format(freq))
                ax1.grid(True)
                ax1.set_ylabel('Signal (units)')
                ax1.set_xlabel('Sample index')
                ax1.plot(range(len(data)), np.real((data)), label='ChA real cl')
                ax1.plot(range(len(data)), np.imag((data)), label='ChA imag cl')
                #ax1.set_ylim(-1, 1)
                ax1.legend(fontsize=10)

                ax2.grid(True)
                ax2.set_title('rx signal  @ {}Hz'.format(freq))
                ax2.set_ylabel('Signal (units)')
                ax2.set_xlabel('Sample index')
                ax2.plot(range(len(rec_sig_A[i,:])), np.real((rec_sig_A[i,:])), label='ChA Real bs %d gain %d' % (i,txgain))
                #ax2.plot(range(len(rec_sig_A[i,:])), np.imag((rec_sig_A[i,:])), label='ChA Imag bs %d' % i)
                ax2.legend(fontsize=10)
                #ax2.set_ylim(-1, 1)

                
                ax3.grid(True)
                ax3.set_title('Power spectral density @ {}Hz'.format(freq))
                ax3.set_ylabel('PSD [V**2/Hz]')
                ax3.set_xlabel('frequency [Hz]')
                ax3.plot(f1, powerBins, label='power of signal for bs %d gain %d' % (i,rxgain))
                ax3.legend(fontsize=10)

    plt.show()


if __name__ == '__main__':
    main()
