#!/usr/bin/python3
"""
 SOUNDER_TXRX_ONESHOT.py
  Basic channel sounding test:
  It program two Irises in TDD mode (one as a base station node,
  and the other as a UE/client node) to transmit and receive a singal according
  to the following schedule/frame:
    Node 1 (BS) schedule PGRGGGGGGGGGGGGGGGRG
    Node 2 (UE) schedule GGPGGGGGGGGGGGGGGGTG
  where the BS is required to first send a beacon signal (initial "P").
  This beacon consists of a signal that has been pre-loaded to the
  BS FPGA buffer. Similarly, the conjugate of the beacon signal has
  been pre-loaded to the UE's buffer in order to enable any correlation
  operation on the UE side.
  The BS node is configured for recurring triggers,
  i.e., a trigger is used only for every frame.
  On the other hand, the UE will rely on an FPGA-based correlator
  to trigger itself and count frames. Given the delay between base station
  and UE (group delay at front-end, and RF path delay) there is a
  mechanism for adjusting the frame time using setHardwareTime
  which sets the start symbol and start count within the
  symbol at the time of a correlator trigger. This frame-time also
  accounts for the time advance between the UE and base station
  node.
  The pilot signal (currently a programmable repition of a WiFi LTS)
  transmitted from the UE to the BS is pre-loaded into the FPGA buffer
  and is transmitted in symbol 3 of each frame ("P" in third slot).
  The symbol "T" shown in the second-to-last slot corresponds to
  our given signal `streamed` from the host to the UE Iris using a
  separate thread. The UE thus transmits this symbol (configured using
  the txSymNum option) right after sending a pilot.
  
  TDD operation starts with a trigger and run for a single frame. Received pilots and data are
  stored in binary files and they can be inspected using plt_simp.py
  in the IrisUtils folder
  Example:
    python3 SOUNDER_TXRX_ONESHOT.py --bsnode="RF3C000042" --clnode="RF3C000025"
  where bsnode corresponds to a base station node and clnode corresponds
  to a client node.
  OUTPUT:
  The script will generate binary files that can be analyzed using the
  plt_simp.py script in folder PYTHON/IrisUtils/
  IMPORTANT NOTE:
  The client firmware has different features than the base station node
  firmware. Therefore, ONLY bsnode can transmit a beacon whereas ONLY
  cnode can correlate against such beacon. This means it is critical to
  set bsnode to the serial number of a base station node and clnode to the 
  serial number of a client node!
  NOTE ON GAINS:
  Gain settings will vary depending on RF frontend board being used
  If using CBRS:
  rxgain: at 2.5GHz [3:1:105], at 3.6GHz [3:1:102]
  txgain: at 2.5GHz [16:1:81], at 3.6GHz [15:1:81]
  If using only Dev Board:
  rxgain: at both frequency bands [0:1:30]
  txgain: at both frequency bands [0:1:42]
  The code assumes both TX and RX have the same type of RF frontend board.
  Also, currently AGC only supports the CBRS RF frontend. It cannot be used
  with the Iris Dev Board or UHF board
---------------------------------------------------------------------
 JOSHUA MIRAGLIA
 RENEW OPEN SOURCE LICENSE: http://renew-wireless.org/license
---------------------------------------------------------------------
"""
import sys
import os
sys.path.append(sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'utils', 'IrisUtils')))

import random as rd
import threading
import SoapySDR
from SoapySDR import *  # SOAPY_SDR_ constants
from optparse import OptionParser
import numpy as np
import time
import signal
import math
import pdb
import json
import pickle
import scipy.io as sio 
from functools import partial
from type_conv import *
from file_rdwr import *
from generate_sequence import *

class TDDradio():

    def __init__(self, bsnode, clnodes, data_writer, rate=5e6, freq=3.6e9, txgain=80.0, rxgain=70.0, numSamps=512, numSyms=10, txSymNum=1, threshold=1,
                 prefix_length=100, postfix_length=100, both_channels=False, calibrate=False, agc_en=False, record=True) -> None:
        # Save the setup info for the class
        self.rate = rate
        self.freq = freq
        self.txgain = txgain
        self.rxgain = rxgain
        self.numSamps = numSamps # the number of samples to recieve in a given transmission
        self.numSyms = numSyms # the numbers of symbols in a frame of data min = 10
        self.txSymNum = txSymNum # the number of client transmission symbols in a single frame
        self.threshold = threshold
        self.prefix_length = prefix_length
        self.postfix_length = postfix_length
        self.both_channels = both_channels # wether we are using two channels or one
        self.calibrate = calibrate
        self.record = record
        self.symSamp = numSamps + prefix_length + postfix_length # the amount of samples per symbol in the TDD frame schedule
        self.running = False # indicater if rx/tx threads should continue to run
        self.txsig = np.zeros(numSamps).astype(np.complex64)
        self.txzero = np.zeros(numSamps).astype(np.complex64) 

        # initialize the settings for the hardware
        print("setting %s as eNB and %s as UE" % (bsnode, clnodes))
        self.bsdrs = []
        self.bsdrs.append(SoapySDR.Device(dict(serial=bsnode)))
        self.clsdrs = []
        for i in range(len(clnodes)):
            self.clsdrs.append(SoapySDR.Device(dict(serial=clnodes[i])))

        self.data_writer = data_writer

        #############################################
        # Some default sample rates
        #############################################
        for i, sdr in enumerate(self.bsdrs + self.clsdrs):
            info = sdr.getHardwareInfo()
            print("%s settings on device %d" % (info["frontend"], i))
            for ch in [0, 1]:
                sdr.setBandwidth(SOAPY_SDR_TX, ch, 2.5*rate)
                sdr.setBandwidth(SOAPY_SDR_RX, ch, 2.5*rate)
                sdr.setSampleRate(SOAPY_SDR_TX, ch, rate)
                sdr.setSampleRate(SOAPY_SDR_RX, ch, rate)
                #sdr.setFrequency(SOAPY_SDR_TX, ch, freq)
                #sdr.setFrequency(SOAPY_SDR_RX, ch, freq)
                sdr.setFrequency(SOAPY_SDR_TX, ch, 'RF', freq-.75*rate)
                sdr.setFrequency(SOAPY_SDR_RX, ch, 'RF', freq-.75*rate)
                sdr.setFrequency(SOAPY_SDR_TX, ch, 'BB', .75*rate)
                sdr.setFrequency(SOAPY_SDR_RX, ch, 'BB', .75*rate)
                sdr.setAntenna(SOAPY_SDR_RX, ch, "TRX")
                sdr.setDCOffsetMode(SOAPY_SDR_RX, ch, True)

                if "CBRS" in info["frontend"]:
                    # Set gains to high val (initially)
                    if agc_en: rxgain = 100
                    sdr.setGain(SOAPY_SDR_TX, ch, txgain)
                    sdr.setGain(SOAPY_SDR_RX, ch, rxgain)
                else:
                    # No CBRS board gains, only changing LMS7 gains
                    # AGC only supported for CBRS boards
                    agc_en = False
                    sdr.setGain(SOAPY_SDR_TX, ch, "PAD", txgain)    # [0:1:42]
                    sdr.setGain(SOAPY_SDR_TX, ch, "IAMP", 0)        # [-12:1:3]
                    sdr.setGain(SOAPY_SDR_RX, ch, "LNA", rxgain)    # [0:1:30]
                    sdr.setGain(SOAPY_SDR_RX, ch, "TIA", 0)         # [0, 3, 9, 12]
                    sdr.setGain(SOAPY_SDR_RX, ch, "PGA", -10)       # [-12:1:19]

                # Read initial gain settings
                readLNA = sdr.getGain(SOAPY_SDR_RX, 0, 'LNA')
                readTIA = sdr.getGain(SOAPY_SDR_RX, 0, 'TIA')
                readPGA = sdr.getGain(SOAPY_SDR_RX, 0, 'PGA')
                print("INITIAL GAIN - LNA: {}, \t TIA:{}, \t PGA:{}".format(readLNA, readTIA, readPGA))


                sdr.writeSetting("TX_SW_DELAY", str(30))
                sdr.writeSetting("TDD_MODE", "true")
            for ch in [0, 1]:
                if calibrate:
                    sdr.writeSetting(SOAPY_SDR_RX, ch, "CALIBRATE", 'SKLK')

            sdr.writeSetting("RESET_DATA_LOGIC", "")

        # Initialize streams for recieving and transmitting
        self.rxStreamCL = []
        self.txStreamCL = []
        for i in range(len(clnodes)):
            self.rxStreamCL.append(self.clsdrs.setupStream(SOAPY_SDR_RX, SOAPY_SDR_CF32, [0, 1]))
            self.txStreamCL.append(self.clsdrs.setupStream(SOAPY_SDR_TX, SOAPY_SDR_CF32, [0, 1]))

        self.rxStreamBS = self.bsdr.setupStream(SOAPY_SDR_RX, SOAPY_SDR_CS16, [0, 1])

        # Adjust settings for amplifier control
        tpc_conf = {"tpc_enabled" : False}
        self.clsdrs.writeSetting("TPC_CONFIG", json.dumps(tpc_conf))
        agc_conf = {"agc_enabled" : agc_en}
        self.bsdr.writeSetting("AGC_CONFIG", json.dumps(agc_conf))

        #################################
        # Setup the beacon
        #################################
        # preambles to be sent from BS and correlated against in UE
        # the base station may upsample, but the mobiles won't
        upsample=1
        preambles_bs = generate_training_seq(preamble_type='gold_ifft', seq_length=128, cp=0, upsample=1)
        # the correlators can run at lower rates, so we only need the downsampled signal.
        preambles = preambles_bs[:, ::upsample]

        ampl = 0.5
        beacon = preambles[0, :]
        coe = cfloat2uint32(np.conj(beacon), order='QI')     # FPGA correlator takes coefficients in QI order
        ltsSym, _ = generate_training_seq(preamble_type='lts', cp=32, upsample=1)
        # ltsSym = lts.genLTS(upsample=1, cp=0)
        pad1 = np.array([0]*(prefix_length), np.complex64)   # to comprensate for front-end group delay
        pad2 = np.array([0]*(postfix_length), np.complex64)  # to comprensate for rf path delay
        wb_pilot = np.tile(ltsSym, numSamps//len(ltsSym)).astype(np.complex64)*ampl
        wbz = np.array([0]*(self.symSamp), np.complex64)
        wb_pilot1 = np.concatenate([pad1, wb_pilot, pad2])
        wb_pilot2 = wbz  # wb_pilot1 if both_channels else wbz

        beacon_weights = np.eye(2, dtype=np.uint32)

        # write the beacon information to the sdr registers
        self.bsdr.writeRegisters("BEACON_RAM", 0, cfloat2uint32(beacon, order='QI').tolist())
        self.bsdr.writeRegisters("BEACON_RAM_WGT_A", 0, beacon_weights[0].tolist())
        self.bsdr.writeRegisters("BEACON_RAM_WGT_B", 0, beacon_weights[1].tolist())
        self.bsdr.writeSetting("BEACON_START", '1')

        ##########################################################
        # Configure the TDD scheduling
        ##########################################################
        # The schedules are setup for the initial sending of the pilot signal from the base station
        # acting as a beacon, which is then followed by a transmission lasting txSymNum symbols in the frame 
        # by the client boards to be recieved by the base station
        base_sched = "BGR"+''.join("G"*(numSyms-txSymNum-4))+''.join("R"*txSymNum)+"G"
        client_sched = "GGP"+''.join("G"*(numSyms-txSymNum-4))+''.join("T"*txSymNum)+"G"
    
        print("Basestation schedule %s " % base_sched) 
        print("Client schedule %s " % client_sched)

        bconf = {"tdd_enabled": True,
                "frame_mode": "free_running",
                "symbol_size": self.symSamp,
                "frames": [base_sched],
                "beacon_start" : prefix_length,
                "beacon_stop" : prefix_length+len(beacon),
                "max_frame" : 1}
        cconf = {"tdd_enabled": True,
                "frame_mode": "continuous_resync",
                "dual_pilot": both_channels,
                "symbol_size" : self.symSamp,
                "frames": [client_sched],
                "max_frame" : 1}

        self.bsdr.writeSetting("TDD_CONFIG", json.dumps(bconf))

        for i in range(len(clnodes)): ##TODO: seperate to make different schedules for each client
            self.clsdrs[i].writeSetting("TDD_CONFIG", json.dumps(cconf))

        ###################################################
        # Configure client pilot signal
        ###################################################
        # the pilot wave for the client will respond to the beacon from tehe base station with a LTS training sequence
        self.clsdrs.writeRegisters("TX_RAM_A", 0, cfloat2uint32(wb_pilot1, order='QI').tolist())

        ########################################
        # Setup client settings
        ########################################
        for i in range(len(clnodes)):
            # since we are not using triggers we have to set up a correlator between base station and clients
            corr_conf = {"corr_enabled" : True, "corr_threshold" : threshold}
            self.clsdrs[i].writeSetting("CORR_CONFIG", json.dumps(corr_conf))
            self.clsdrs[i].writeRegisters("CORR_COE", 0, coe.tolist())

            # setup the initial timing for the client device for scheduling
            # corr delay is 17 cycles
            ueTrigTime = len(beacon) + 200 # prefix_length + len(beacon) + postfix_length + 17 + tx_advance + 150
            sf_start = ueTrigTime // self.symSamp
            sp_start = ueTrigTime % self.symSamp
            print("UE starting symbol and sample count (%d, %d)" % (sf_start, sp_start))
            # make sure to set this after TDD mode is enabled "writeSetting("TDD_CONFIG", ..."
            self.clsdrs[i].setHardwareTime(SoapySDR.ticksToTimeNs((sf_start << 16) | sp_start, rate), "TRIGGER")

            # Start the correlation
            self.clsdrs[i].writeSetting("CORR_START", "A")

        
    """
        Start the process of sending signals sending signals from the clients to the base station
        and recording a list of signals inputs along the way
    """
    def start_recording(self):
        #trigger the base station to send out the first braodcasting signal
        self.bsdr.writeSetting("TRIGGER_GEN", "")
        for i in range(len(self.clnodes)):
            txth = threading.Thread(target=self.tx_thread, args=(i))
            txth.start()

        #spin up recording threads
        rxth = threading.Thread(target=self.mimo_record_thread, args=())
        rxth.start()

        num_trig = 0
        while True:
            time.sleep(1)
            t = SoapySDR.timeNsToTicks(self.clsdrs.getHardwareTime(""),self.rate) >> 32 #trigger count is top 32 bits.
            print("%d new triggers, %d total" % (t - num_trig, t))
            num_trig = t

    """
        This function will send a signal from the client device to the base array 
        and record a single sample of the full signal
        INPUTS:
            signal (numpy.array): the signal to transmit to the base array should be of length numSamp
        RETURN:
            the recieved signal at the base array
    """
    def send_recieve_sig(self, signal):
        
        if (len(signal) != self.txSymNum*self.numSamps):
            print("signal was not of the right size, should be of size %d" % self.txSymNum*self.numSamps)
            return -1

        # trigger the base station to start or resynce and then create the tx thread for the client to synchronize and transmit the signal
        self.bsdr.writeSetting("TRIGGER_GEN", "")
        txth = threading.Thread(target=tx_thread, args=(self.clsdrs, self.rate, self.txStreamCL, self.rxStreamCL, signal, self.numSamps, self.numSyms, self.txSymNum, self.numSyms-self.txSymNum-1))
        txth.start()

        ######################################################
        # recieve the signal from the base station
        ########################################################
        # prep buffers to read 
        waveRxA = np.array([0]*self.symSamp, np.uint32)
        waveRxB = np.array([0]*self.symSamp, np.uint32)

        # activate the rx stream for the base station
        flags = 0
        r1 = self.bsdr.activateStream(self.rxStreamBS, flags, 0)
        if r1<0:
            print("Problem activating stream #1")

        ######################################################
        # Retrieve the incoming signal
        ######################################################
        ret_sig_A = np.array([0]*self.symSamp, np.uint32)
        ret_sig_B = np.array([0]*self.symSamp, np.uint32)

        # First read the pilot signal from client channel A
        sr = self.bsdr.readStream(self.rxStreamBS, [waveRxA, waveRxB], self.symSamp)
        #print("BS: readStream returned %d" % sr.ret)
        if sr.ret < 0 or sr.ret > self.symSamp:
            print("BS - BAD: readStream returned %d"%sr.ret)

        # Read the transmission signals from the client
        for j in range(self.txSymNum):
            sr = self.bsdr.readStream(self.rxStreamBS, [waveRxA, waveRxB], self.symSamp)
            if sr.ret < 0 or sr.ret > self.symSamp:
                print("BS: readStream returned %d"%sr.ret)
            
            # add he next portion of the returned signal to the total signal
            ret_sig_A[(j*self.symSamp):((j+1)*self.symSamp)] = waveRxA
            ret_sig_B[(j*self.symSamp):((j+1)*self.symSamp)] = waveRxB
            

        # deactivate the base station rx stream
        self.bsdr.deactivateStream(self.rxStreamBS)

        return ret_sig_A, ret_sig_B


    def tx_thread(self): #self.clsdrs, self.rate, self.txStreamCL, self.rxStreamCL, sig, self.symSamp, self.numSyms, txSymNum, numSyms-txSymNum-1
        # prep streams for getting time and transmission
        flags = 0
        self.clsdrs.activateStream(self.txStreamCL)
        self.clsdrs.activateStream(self.rxStreamCL, flags, 0)

        # get the the time from the recieving radio
        waveRxA = np.array([0]*self.numSamps, np.uint32) # empty arrays for the readstream function
        waveRxB = np.array([0]*self.numSamps, np.uint32)

        sr = self.clsdrs.readStream(self.rxStreamCL, [waveRxA, waveRxB], self.numSamps)
        print("CL: readStream returned %d, flags %d" % (sr.ret, sr.flags))
        startSymbol = self.numSyms-self.txSymNum-1 # This indicated the start symbol for the transmission broadcasting
        if sr.ret > 0:
            txTime = sr.timeNs & 0xFFFFFFFF00000000
            txTime += (0x000000000 + (startSymbol << 16)) # shift the transmission time to the first transmission signal of the TDD schedule
            print("receive time 0x%X" % sr.timeNs)
            print("transmit time 0x%X" % txTime)
        else:
            # exit thread if not able to sync up times
            return

        # stream the transmission signal at the appropriate time calculate above
        flags = SOAPY_SDR_HAS_TIME # | SOAPY_SDR_END_BURST
        for j in range(self.txSymNum):
            txTimeNs = txTime      # SoapySDR.ticksToTimeNs(txTime, rate)
            st = self.clsdrs.writeStream(self.txStreamCL, [self.txsig, self.txzero], self.numSamps, flags, timeNs=txTimeNs)
            # sts = sdr.readStreamStatus(txStream)
            txTime += 0x10000
            if st.ret < 0:
                print("ret=%d,flags=%d,timeNs=0x%X,txTime=0x%X" % (st.ret, st.flags, st.timeNs, txTime))
        
    def mimo_record_thread(self, rec_index):
        
        rxFrNum = 0
        pilotSymNum = 1
        waveRxA = np.array([0]*self.numSamps, np.uint32)
        waveRxB = np.array([0]*self.numSamps, np.uint32)
        flags = 0
        r1 = self.bsdr.activateStream(self.rxStreamCL, flags, 0)
        if r1<0:
            print("Problem activating stream #1")
        while (self.running):
            for j in range(pilotSymNum):
                sr = self.bsdr.readStream(self.rxStreamCL, [waveRxA, waveRxB], self.numSamps)
                #print("BS: readStream returned %d" % sr.ret)
                if sr.ret < 0 or sr.ret > self.numSamps:
                    print("BS - BAD: readStream returned %d"%sr.ret)
                for i, a in enumerate(waveRxA):
                    pickle.dump(a, fip)

            for j in range(self.txSymNum):
                sr = self.bsdr.readStream(self.rxStreamCL, [waveRxA, waveRxB], self.numSamps)
                if sr.ret < 0 or sr.ret > self.numSamps:
                    print("BS: readStream returned %d"%sr.ret)
                for i, a in enumerate(waveRxA):
                    pickle.dump(a, fid)

            rxFrNum += 1
        fip.close()
        fid.close()

        self.bsdr.deactivateStream(self.rxStreamCL)
        self.bsdr.closeStream(self.rxStreamCL)
        print("Exiting RX Thread, Read %d Frames" % rxFrNum)


    def __del__(self):
        # clean up the sdr deivces for bot the base station and the client
        corr_conf = {"corr_enabled" : False}
        self.clsdrs.writeSetting("CORR_CONFIG", json.dumps(corr_conf))

        tpc_conf = {"tpc_enabled" : False}
        self.clsdrs.writeSetting("TPC_CONFIG", json.dumps(tpc_conf))

        # close streams
        self.clsdrs.closeStream(self.rxStreamCL)
        self.clsdrs.closeStream(self.txStreamCL)
        self.bsdr.closeStream(self.rxStreamBS)

        # ADC_rst, stops the tdd time counters
        tdd_conf = {"tdd_enabled" : False}
        for sdr in [self.bsdr, self.clsdrs]:
            sdr.writeSetting("RESET_DATA_LOGIC", "")
            sdr.writeSetting("TDD_CONFIG", json.dumps(tdd_conf))
            sdr.writeSetting("TDD_MODE", "false")

        agc_conf = {"agc_enabled" : False}
        self.clsdrs.writeSetting("AGC_CONFIG", json.dumps(agc_conf))

        self.bsdr = None
        self.clsdrs = None


    """
        Function that returns the amount of samples that a transmitted signal should contain
    """
    def get_tx_sig_length(self):
        return self.txSymNum*self.numSamps


