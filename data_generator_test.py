import sys
import os

from h5py._hl import dataset
import numpy as np
import h5py
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import collections
import time
from subprocess import Popen, PIPE
import glob

from sqlalchemy import true


from utils.data_tools import CNNDataGenerator

def main():
    ###############################################
    # Parse optional parameters
    ##################################################
    
    # run data generator and create some graphs
    train_gen, val_gen = CNNDataGenerator.create_train_validate_data_generators("F:\\POWDER\\processed", 64, 10000, min_snr=5)

    batch_samples, batch_lables = train_gen.__getitem__(0)

    batch_samples, batch_lables = val_gen.__getitem__(0)


    # run data generator with reference and mod label and create some graphs
    train_gen, val_gen = CNNDataGenerator.create_train_validate_data_generators("F:\\POWDER\\processed", 64, 10000,include_reference=True, include_mod_label=False)

    batch_samples, batch_lables = train_gen.__getitem__(0)

    batch_samples, batch_lables = val_gen.__getitem__(0)

    # run data generator with reference and mod label and create some graphs
    train_gen, val_gen = CNNDataGenerator.create_train_validate_data_generators("F:\\POWDER\\processed", 64, 10000,include_reference=True, include_psd=True)

    batch_samples, batch_lables = train_gen.__getitem__(0)

    batch_samples, batch_lables = val_gen.__getitem__(0)

    pass

    
    

if __name__ == '__main__':
    scrpt_strt = time.time()
    main()
    scrpt_end = time.time()
    print(">>>> Script Duration: time: %f \n" % ( scrpt_end - scrpt_strt) )