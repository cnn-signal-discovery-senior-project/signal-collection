"""
    sig_gen.py
    methods for generating signal data
"""
 
import numpy as np

import os
import h5py


"""
    generate a single sine waveform of a given frequency

    INPUTS:
        freq (int Hz): the frequency of the sine signal
        rate (int Hz): the sampling rate of the radio the signal is being generated for
        signal_size (int): length of a signal
    OUTPUTS:
        data (np complex64 array): the array containg the sine signal
"""
def gen_sine_sig(freq, amp, rate, signal_size):
    Ts = 1/rate # the time steps given a certain sampling rate
    s_time_vals = np.array(np.arange(0, signal_size)).transpose()*Ts
    data = amp*np.exp(s_time_vals*1j*2*np.pi*freq).astype(np.complex64)

    return data


"""
    generate a multiple  sine waveform of a random frequencies and return the signal and its properties

    INPUTS:
        freq (int Hz): the frequency of the sine signal
        rate (int Hz): the sampling rate of the radio the signal is being generated for
        signal_size (int): length of a signal
    OUTPUTS:
        data ((np.complex64) array, (int) frequcny of sine, (int) bandwidth of sine, (float) amplitude of signal)
"""
def gen_mult_sine_sig(rate, signal_size, num_sigs):
    for i in range(num_sigs):
        freq = np.random.randint(10e3,1e6)
        amp = np.random.randint(1e5,1e6)/10e6
        yield gen_sine_sig(freq, amp, rate, signal_size), freq, 0, amp



"""
load and return signals that were generated from a file and put into the gen_data folder
INPUTS:

OUTPUTS:
    yields returns a list of generated signals, which are of complex np type
"""
def sig_gen_from_file(config_params):
    base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) # directory of the experiment
    data_dir = os.path.join(base_dir, "gen_data")
    data_files = os.listdir(data_dir)

    for data_file in data_files:
        if data_file.endswith(".h5"):
            data_file_path = os.path.join(data_dir, data_file)
            with h5py.File(data_file_path, 'r') as data_in:
                gen_sigs = data_in['generated_data']['dataset']
                gen_cent_freqs = data_in['generated_data']['centers']
                gen_bandwidths = data_in['generated_data']['bandwidths']
                for i, sig in enumerate(gen_sigs):
                    yield [sig, gen_cent_freqs[i], gen_bandwidths[i], 0]
    
    return

def rand_sig_from_file(file):
    with h5py.File(file, 'r') as data_in:
        rand_index = np.random.randint(0, 10000)

        gen_sig = data_in['generated_data']['dataset'][rand_index]
        gen_cent_freq = data_in['generated_data']['centers'][rand_index]
        gen_bandwidth = data_in['generated_data']['bandwidths'][rand_index]
        
        return gen_sig, gen_cent_freq, gen_bandwidth





