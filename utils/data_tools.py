import h5py
import os

import numpy as np
import sklearn
from sklearn.utils import shuffle
from tensorflow.keras.utils import Sequence
from sklearn.preprocessing import normalize

class experiment_dataset():
    def __init__(self, sig_len, num_sigs, num_clients, num_elements, sample_rate=5e5, transmit_freq = 3.55e6, tx_signals=None, file_name='out_data.h5', in_metadata=None, blank=False) -> None:
        ##########################################################
        # Initilize teh hdf5 file if it doesn't exist
        ##########################################################

        # there is an option to create a blank dataset in case you want to add parameters manually or load one
        # in yourself
        if blank:
            # initialize a blank dataset
            self.data_out = None
            return

        base_dir = os.path.dirname(os.path.abspath(__package__)) # directory of the experiment
        fout = os.path.join(base_dir, file_name)
        
        try:
            self.data_out = h5py.File(fout,'x')
        except OSError:
            # file exists so instead reading the file in
            self.data_out = h5py.File(fout,'r+')

        dt = h5py.special_dtype(vlen=str)
        self.description = self.data_out.create_dataset('description', (1,), dtype=dt)
        desctiption = str("""
            This file holds a collection of labled signals collected by a mMIMO array for the use in machine learning training.
            any given index which corresponsds to the signal id can be used to grab the signal that was transmitted (signals/tx_signals), the retrieved signal
            at each mMIMO element (signals/rx_signals), the recieved reference signal (signal tramistted from angle 0, signals/rx_reference),
            the noise reference for that signal (signals,reference). signal datasets have the following shapes, for rx_signals/rx_reference (num_elements, num_sigs, sig_len),
            for tx_signals/reference (num_sigs, sig_len). The signals id curesponds to the axes with num_sigs. This id will alos grab the signals
            assoociated labels in the labels group. All data within this dataset was taken at the same location and sampling rate, which is provided in the 
            metadata group. Further miscellaneous parameters can be found in signals group which relate to how the signals are captured and stored.
        """)

        self.description[0] = desctiption
        self.data_out.attrs['description'] = desctiption


        signals = self.data_out.create_group('signals')
        self.rx_sig = signals.create_dataset('rx_signals', (num_elements, num_sigs, sig_len), maxshape=(num_elements, None, sig_len),dtype='c16', chunks = (1,1,2048), compression="lzf", shuffle=True)
        self.rx_ref = signals.create_dataset('rx_reference', (num_elements, num_sigs, sig_len), maxshape=(num_elements, None, sig_len),dtype='c16',chunks = (1,1,2048), compression="lzf", shuffle=True)
        self.tx_sig = signals.create_dataset('tx_signals', (num_sigs, sig_len), maxshape=(None, sig_len), dtype='c16',chunks = (1,2048), compression="lzf", shuffle=True)
        self.ref_sig = signals.create_dataset('reference', (num_sigs, sig_len), maxshape=(None, sig_len),dtype='c16',chunks = (1,2048), compression="lzf", shuffle=True)

        # make the signal counters for signal indexing
        self.frame_ind = 0

        labels = self.data_out.create_group('labels')
        self.center_freq = labels.create_dataset('center_freq',(num_sigs,), maxshape=(None,), dtype=float)
        self.bandwidth = labels.create_dataset('bandwidth', (num_sigs,), maxshape=(None,), dtype=float)
        self.start_index = labels.create_dataset('start_index',(num_sigs,), maxshape=(None,), dtype=float)
        self.end_index = labels.create_dataset('end_index',(num_sigs,), maxshape=(None,), dtype=float)
        self.SNR = labels.create_dataset('SNR',(num_sigs,), maxshape=(None,), dtype=float)
        self.modulation = labels.create_dataset('modulation',(num_sigs,), maxshape=(None,), dtype=dt)


        # create the metadata datasets which shouldn't change 
        metadata = self.data_out.create_group('metadata')
        mimo_spatial = metadata.create_dataset('mimo_coord', (3,), dtype=float)
        client_spatial = metadata.create_dataset('client_coord', (3,), dtype=float)
        reference_spatial = metadata.create_dataset('reference_coord', (3,), dtype=float)
        azimuth_angle = metadata.create_dataset('azimuth_angle', (1,), dtype=float)
        elevation_angle = metadata.create_dataset('elevation_angle', (1,), dtype=float)
        sample_rate = metadata.create_dataset('sample_rate', (1,), dtype=float)

        # set data file attributes
        signals.attrs['num_frames'] = num_sigs
        signals.attrs['signal_len'] = sig_len
        signals.attrs['num_elements'] = num_elements
        signals.attrs['num_clients'] = num_clients

        # if user provided metadata use that to fill in more information
        if in_metadata is not None:
            # signal attributes
            signals.attrs['bs_ant_num'] = in_metadata['BS_ANT_NUM_PER_CELL']
            signals.attrs['bs_hub_id'] = in_metadata['BS_HUB_ID']
            signals.attrs['bs_rx_gain'] = in_metadata['BS_RX_GAIN_A']
            signals.attrs['bs_sdr_id'] = in_metadata['BS_SDR_ID']

            signals.attrs['cl_tx_gain'] = in_metadata['CL_TX_GAIN_A'][0]
            
            signals.attrs['center_freq'] = in_metadata['FREQ']


            # experiment parameters
            azimuth_angle[0] = in_metadata['AZIMUTHAL_ANGLE']
            elevation_angle[0] = in_metadata['ELEVATION_ANGLE']
            client_spatial[:] = in_metadata['CLIENT_COORDINATES']
            reference_spatial[:] = in_metadata['REFERENCE_COORDINATES']
            mimo_spatial[:] = in_metadata['MIMO_COORDINATES']

            # calculate angles for the client
            # first shift the mimo to be at the origin
            cc = in_metadata['CLIENT_COORDINATES'] - in_metadata['MIMO_COORDINATES']
            
            azimuth_angle[0] = np.arctan(cc[0]/cc[1])
            elevation_angle[0] = np.arctan(np.sqrt(cc[0]**2 + cc[1]**2)/cc[2])

        # fill some of the datasets with preset values
        self.sample_rate = sample_rate
        
        if tx_signals is not None:
            self.tx_sig = tx_signals
        else:
            print('no tx_signals given not saving them to data file')


        # Fill out some misc properties that are usful later
        self.sig_len = sig_len
        self.num_sigs = num_sigs
        self.num_elements = num_elements
        self.num_clients = num_clients


    """
        Initialize a dataset from an existing hdf5 dataset

        INPUT: 
            dataset_path (string): path to dataset

        RETURN:
            None
    """
    def load_from_dataset(self, dataset_path):
        try:
            self.data_out = h5py.File(dataset_path,'x')
        except OSError:
            # file exists so instead reading the file in
            self.data_out = h5py.File(dataset_path,'r+')


        # fill in member variables with fields from the dataset
        signals = self.data_out['signals']
        self.rx_sig = signals['rx_signals']
        self.rx_ref = signals['rx_reference']
        self.tx_sig = signals['tx_signals']
        self.ref_sig = signals['reference']


        labels = self.data_out['labels']
        self.center_freq = labels['center_freq']
        self.bandwidth = labels['bandwidth']
        self.start_index = labels['start_index']
        self.end_index = labels['end_index']
        self.SNR = labels['SNR']
        self.modulation = labels['modulation']


        # Fill out some misc properties that are usful later
        self.sig_len = signals.attrs['signal_len']
        self.num_sigs = signals.attrs['num_frames']
        self.num_elements = signals.attrs['num_elements']
        self.num_clients = signals.attrs['num_clients']
        self.sample_rate = self.data_out['metadata/sample_rate'][0]
        self.frame_ind = signals.attrs['num_frames']

    """
        Initialize the training dataset using the experiment configuration file

        INPUT:
            exp_conf: experiment configuration path
    """
    #def __init__(self, exp_conf):
    #    with open(os.path.join(exp_conf)) as config_file:
    #        data = json.load(config_file)
    #        #TODO: finish
#
#
    #
    #"""
    #    Iniitiliaze a trainign dataset with an existing one
#
    #    INPUT: 
    #        training_dataset: file path to a hdf5 file containing a trainign dataset
    #
    #"""
    #def __init__(self, training_dataset):
    #    self.data_out = h5py.File(training_dataset, 'rw')
#
    #    # initilize class variables to different datasets in the file
    #    self.rx_sig = self.data_out['signals/rx_signals']
    #    self.tx_sig = self.data_out['signals/tx_signals']
    #    self.ref_sig = self.data_out['signals/reference']
#
    #    self.center_freq = self.data_out['labels/center_freq']
    #    self.bandwidth = self.data_out['labels/bandwidth']
    #    self.start_index = self.data_out['labels/start_index']
    #    self.end_index = self.data_out['labels/end_index']
    #    self.SNR = self.data_out['labels/SNR']
    #    self.modulation = self.data_out['labels/modulation']
#
    #    # set variables from data file attributes
    #    self.frame_ind = self.data_out.attrs['num_frames']
    #    self.sig_len = self.data_out.attrs['signal_len']
    #    self.num_sigs = self.data_out.attrs['num_frames']
    #    self.num_elements = self.data_out.attrs['num_elements'] 
    #    self.num_clients = self.data_out.attrs['num_clients']





    """
    Save an frame of rx signals, a frame is composed of all of the recieved signals from a 
    single frame, the transmitted signal, a noise reference signal, and all labels needed for training with a tranmitted siganl 

    INPUTS:
        rx_sigs: complex numpy array of size (num_bs, num_cl, sig_len)
        tx_sig: complex numpy array of shape (sig_len,) that is the transmitted signal for a frame
        ref_sig: complex numpy array of shape (sig_len,) that is a noise reference for a given frame
        rx signal data labels
    """
    def save_frame(self, rx_sigs, tx_sig, ref_sig, center_freq, bandwidth, start_ind, end_ind, SNR, mod):
        # check to see if data file needs to be resized
        if self.frame_ind >= self.num_sigs:
            # resize all datasets that use sig_size and change the number of signal size of the class 
            self.__increase_signal_cap()

        # save the tx signal
        if len(tx_sig) != self.sig_len:
            print('tx_sig is the wrong size filling tx_sig for frame %d with 0')
            self.tx_sig[self.frame_ind, :] = np.zeros((self.sig_len,), dtype=np.complex64)
        else:
            self.tx_sig[self.frame_ind, :] = tx_sig

        # save the ref signal
        if len(ref_sig) != self.sig_len:
            print('ref_sig is the wrong size filling ref_sig for frame %d with 0')
            self.ref_sig[self.frame_ind, :] = np.zeros((self.sig_len,), dtype=np.complex64)
        else:
            self.ref_sig[self.frame_ind, :] = ref_sig

        ######################################################
        # save all of the rx signals
        #######################################################
        # check rx_sigs shape to see if all necesary data is given
        if rx_sigs.shape != (self.num_elements, self.num_clients, self.sig_len):
            print('rx_sigs is not the right shape it is ({}) should be shape ({},{},{})'.format( rx_sigs.shape, self.num_elements, self.num_clients, self.sig_len))

        # iterate through all signals and save them to their slot in the data file
        self.rx_sig[:, self.frame_ind, :] = rx_sigs[:,0,:]
        self.rx_ref[:, self.frame_ind, :] = rx_sigs[:,1,:]
      
        #############################################################
        # save  data labels now
        #############################################################
        self.center_freq[self.frame_ind] = center_freq
        self.bandwidth[self.frame_ind] = bandwidth
        self.start_index[self.frame_ind] = start_ind
        self.end_index[self.frame_ind] = end_ind
        self.SNR[self.frame_ind] = SNR
        self.modulation[self.frame_ind] = mod

        # incriment the frame index
        self.frame_ind += 1

    """
    Save multiple frames of rx signals, a frame is composed of all of the recieved signals from a 
    single frame, the transmitted signal, a noise reference signal, and all labels needed for training with a tranmitted siganl 

    INPUTS:
        rx_sigs: complex numpy array of size (num_bs, num_cl, num_frames, sig_len)
        tx_sig: complex numpy array of shape (num_frames,sig_len) that is the transmitted signal for a frame
        ref_sig: complex numpy array of shape (num_frames,sig_len) that is a noise reference for a given frame
        rx signal data labels
    """
    def save_frames(self, num_frames, rx_sigs, tx_sig, ref_sig, center_freq, bandwidth, start_ind, end_ind, SNR, mod):
        # check to see if data file needs to be resized
        if self.frame_ind+num_frames >= self.num_sigs:
            # resize all datasets that use sig_size and change the number of signal size of the class 
            self.__increase_signal_cap(num_frames)

        # save the tx signal
        end_frame_ind = self.frame_ind+num_frames
        if tx_sig.shape != (num_frames,self.sig_len):
            print('tx_sig is the wrong size filling tx_sig for frames %d,%d with 0', self.frame_ind,end_frame_ind)
            self.tx_sig[self.frame_ind:end_frame_ind, :] = np.zeros((num_frames,self.sig_len), dtype=np.complex64)
        else:
            self.tx_sig[self.frame_ind:end_frame_ind, :] = tx_sig

        # save the ref signal
        if tx_sig.shape != (num_frames,self.sig_len):
            print('tx_sig is the wrong size filling reference sig for frames %d,%d with 0', self.frame_ind,end_frame_ind)
            self.ref_sig[self.frame_ind:end_frame_ind, :] = np.zeros((num_frames,self.sig_len), dtype=np.complex64)
        else:
            self.ref_sig[self.frame_ind:end_frame_ind, :] = ref_sig

        ######################################################
        # save all of the rx signals
        #######################################################
        # check rx_sigs shape to see if all necesary data is given
        if rx_sigs.shape != (self.num_elements, self.num_clients, self.sig_len):
            print('rx_sigs is not the right shape it is (%d) should be shape (%d,%d,%d)', rx_sigs.shape, self.num_elements, self.num_clients, self.sig_len)

        # iterate through all signals and save them to their slot in the data file
        self.rx_sig[:, :, self.frame_ind, :] = rx_sigs
      
        #############################################################
        # save  data labels now
        #############################################################
        assert center_freq.shape == (num_frames,)
        assert bandwidth.shape == (num_frames,)
        assert start_ind.shape == (num_frames,)
        assert end_ind.shape == (num_frames,)
        assert SNR.shape == (num_frames,)
        assert mod.shape == (num_frames,)
        self.center_freq[self.frame_ind:end_frame_ind] = center_freq
        self.bandwidth[self.frame_ind:end_frame_ind] = bandwidth
        self.start_index[self.frame_ind:end_frame_ind] = start_ind
        self.end_index[self.frame_ind:end_frame_ind] = end_ind
        self.SNR[self.frame_ind:end_frame_ind] = SNR
        self.modulation[self.frame_ind:end_frame_ind] = mod

        # incriment the frame index
        self.frame_ind += num_frames


    def __increase_signal_cap(self, num_signals=1):
        self.num_sigs += num_signals
        self.data_out.attrs['num_frames'] = self.num_sigs
 
        #resize all datasets that use number of signals according to the new signal size
        self.ref_sig.resize((self.num_sigs, self.sig_len))
        self.tx_sig.resize((self.num_sigs, self.sig_len))
        self.rx_sig.resize((self.num_elements, self.num_sigs, self.sig_len))
        self.rx_ref.resize((self.num_elements, self.num_sigs, self.sig_len))


        self.center_freq.resize((self.num_sigs,))
        self.bandwidth.resize((self.num_sigs,))
        self.start_index.resize((self.num_sigs,))
        self.end_index.resize((self.num_sigs,))
        self.SNR.resize((self.num_sigs,))
        self.modulation.resize((self.num_sigs,))



    def close(self):
        self.data_out.close()

    def __del__(self):
        self.data_out.close()


class CNNDataGenerator(Sequence):

    def __init__(self, in_data_root, batch_size, num_samples=None, max_recursion_lvls=3, include_reference=False,
                 include_psd=False, num_bs_sigs_per_input=1, include_mod_label=False, normalize=True):
        self.batch_size = batch_size
        self.include_reference = include_reference
        self.include_psd = include_psd
        self.num_bs_sigs_per_input = num_bs_sigs_per_input
        self.include_mod_label = include_mod_label

        self.num_samples = num_samples
        self.normalize = normalize

        self.snr_scaling = 50

        ###########################################################
        # Create database of usable input signals
        ###########################################################

        # check in in data root is a directory or database of already processed datasets
        # if direcoty process it like normal and create a data generator from it, if 
        # it is a database process fast and return

        if type(in_data_root) is dict:
            # process in_data_root as a database of already processesd datasets
            # processed datasets should only have come from create_train_validate_data_generators()
            # where we know it gives already checked values so fill in other needed values and return
            self.sample_info = in_data_root['sample_info']
            self.datasets = in_data_root['datasets']
            self.dataset_gains = in_data_root['dataset_gains']
            self.sample_idx = in_data_root['sample_idx']

            return

        ###########################################################
        # populate batabase with diretories within the given root   
        #########################################################

        # checks for propery inputs
        if not os.path.isdir(in_data_root):
            print("invalid input directory {}, need to give a directory as the in-data-root parameter to perform search".format(in_data_root))
            return None

        ##################################################################################
        # Cycle through directories or single file to process and store training dataset
        ##################################################################################
        self.sample_info = []
        self.datasets = []
        self.dataset_gains = []
        self.dataset_num_samples = []
        self.dataset_num_bs_elm = []
        file_idx = 0
        for root, dirs, files in os.walk(in_data_root, topdown=True):
            for f in files:
                if f.endswith(".hdf5"):
                    cur_file = os.path.join(root, f)
                    data_out = h5py.File(cur_file,'r')

                    self.datasets.append(data_out)
                    self.dataset_num_samples.append(data_out['signals'].attrs['num_frames'])
                    self.dataset_num_clients.append(data_out['signals'].attrs['num_clients'])
                    self.dataset_num_bs_elm.append(data_out['signals'].attrs['num_elements'])

                    # check to see if the number of elements exceeds number of bs/cl signals per input requested
                    if data_out['signals'].attrs['num_elements'] < self.num_bs_sigs_per_input:
                        self.num_bs_sigs_per_input = data_out['signals'].attrs['num_elements']

                    # add all possible indexing combintation from file into the samples_idx array
                    for idx in range(data_out['signals'].attrs['num_frames']):
                        self.sample_info.append(np.array([file_idx, idx]))
                        
                    # increment the file index 
                    file_idx += 1

            # enforce max depth
            if root.count(os.sep) - in_data_root.count(os.sep) == max_recursion_lvls:
                del dirs[:] # if dirs is removed then os.walk wont look to go into those folders 


        #convert array to numpy array if usful
        self.sample_info = np.array(self.sample_info)

        #########################################################
        # make and shuffle sample idecies
        self.sample_idx = list(range(np.shape(self.sample_info)[0]))
        self.sample_idx = shuffle(self.sample_idx)
        self.sample_info = self.sample_info[self.sample_idx]

        # enforce number of samples requested by user if specified
        if num_samples is None:
            # get number of samples from files
            self.num_samples = sum(self.dataset_num_samples)

        # check if number of samples is less than total dataset  truncate list of avalible smaple indexes
        if self.num_samples < sum(self.dataset_num_samples):
            self.sample_idx = self.sample_idx[:self.num_samples] 


    @staticmethod
    def create_train_validate_data_generators(in_data_root, batch_size, num_samples=None, max_recursion_lvls=3, include_reference=False,
                include_psd=False, num_bs_sigs_per_input=1, include_mod_label=False, dataset_split=.8, min_snr=None, normalize=True):
        '''
        Create training and validation data generators for keras from collection of datasets corresonding to 
        the blueprint found in the Colleced data format here: https://gitlab.flux.utah.edu/cnn-signal-discovery-senior-project/signal-collection

        Inputs:
            in_data_root -> (path) path to direcoty holding all wanted datasets
            batch_size -> (int) size of the batch size wanted for training and validation
            num_samples -> (int) the combined number of sample wanted in both training and validation datasets
            max_recurion_lvls -> (int) the max depth to search for datasets given a specific in_data_root
            include_reference -> (boolean) included the non moving reference rx signals into the inputs
            include_psd -> (boolean) include extra dimension with PSD of each signal into the inputs
            num_bs_sigs_per_input -> (int) number of bs antenna from MMIMO to include in each input
            include_mod_label -> (boolean) use modulation as the label for categorical inference instead of predicting other signal parameters
            dataset_split -> [0,1] the porporiton of dataset you want allocated to the training data
                            example .6 make 60% of dataset training and 40% validation
            min_snr -> (float) minimum SNR to find in your dataset, default is all
            normalize -> (bolean) normalize input and labels
        Return:
            train_generator -> (keras.utils.Sequence) the generator corresponding to the training part of the dataset
            validation_generator -> (keras.utils.Sequence) the generator corresponding to the validation part of the dataset
        
        '''
        
        ###########################################################
        # populate Dataset with usable subdataset    
        #########################################################

        # checks for propery inputs
        if not os.path.isdir(in_data_root):
            print("invalid input directory {}, need to give a directory as the in-data-root parameter to perform search".format(in_data_root))
            return None


        file_idx = 0
        sample_info = np.empty((0,2), dtype=np.int32)
        datasets = []
        dataset_gains = [] #TODO: track the gains from each dataset
        dataset_num_samples = []
        dataset_num_bs_elm = []
        for root, dirs, files in os.walk(in_data_root, topdown=True):
            for f in files:
                if f.endswith(".hdf5"):
                    cur_file = os.path.join(root, f)
                    data_out = h5py.File(cur_file,'r')

                    datasets.append(data_out)
                    dataset_num_samples.append(data_out['signals'].attrs['num_frames'])
                    #dataset_num_clients.append(data_out['signals'].attrs['num_clients'])
                    dataset_num_bs_elm.append(data_out['signals'].attrs['num_elements'])

                    # check to see if the number of elements exceeds number of bs/cl signals per input requested
                    if data_out['signals'].attrs['num_elements'] < num_bs_sigs_per_input:
                        num_bs_sigs_per_input = data_out['signals'].attrs['num_elements']


                    ########################################################################################
                    # index possible avalible samples

                    # check if SNR requirment is wanted
                    if min_snr != None:
                        # index samples discarding samples which don't fall above the minimum SNR
                        sample_snr = np.array(data_out['labels/SNR'])
                        valid_samples = np.argwhere(sample_snr > min_snr)

                        file_info = file_idx*np.ones(valid_samples.shape, dtype=valid_samples.dtype)
                        info = np.column_stack((file_info, valid_samples))

                        sample_info = np.concatenate((sample_info, info), axis=0)
                        pass
                    else:
                        # add all possible indexing combintation from file into the samples_idx array
                        num_samples_in_file = (data_out['signals'].attrs['num_frames'])
                        valid_samples = np.transpose(np.indices((num_samples_in_file,), dtype=np.uint32))
                        
                        file_info = file_idx*np.ones(valid_samples.shape, dtype=valid_samples.dtype)
                        info = np.column_stack((file_info, valid_samples))

                        sample_info = np.concatenate((sample_info, info), axis=0)
                        
                    # increment the file index 
                    file_idx += 1

            # enforce max depth
            if root.count(os.sep) - in_data_root.count(os.sep) == max_recursion_lvls:
                del dirs[:] # if dirs is removed then os.walk wont look to go into those folders 


        #convert array to numpy array if usful
        sample_info = np.array(sample_info)

        #########################################################
        # make and shuffle sample idecies
        sample_idx = list(range(np.shape(sample_info)[0]))
        sample_idx = shuffle(sample_idx)
        sample_info = sample_info[sample_idx]

        # enforce number of samples requested by user if specified
        if num_samples is None:
            # get number of samples from files
            num_samples = sum(dataset_num_samples)

        # check if number of samples is less than total dataset
        if num_samples < sum(dataset_num_samples):
            # if exceeded truncate list of avalible smaple indexes
            sample_idx = sample_idx[:num_samples] 

        # seperate into training and validation
        split_idx = int(num_samples * dataset_split) # default to a 80/20% split of data
        train_idx = sample_idx[:split_idx]
        validate_idx = sample_idx[split_idx:]

        # create databasees for usful information for train and validation data generators
        train_data_root = {'sample_info': sample_info, 'datasets': datasets, 
                           'dataset_gains': dataset_gains, 'sample_idx': train_idx}

        validate_data_root = {'sample_info': sample_info, 'datasets': datasets, 
                           'dataset_gains': dataset_gains, 'sample_idx': validate_idx}


        # create data generators for train and validation portions
        train_generator = CNNDataGenerator(train_data_root, batch_size, num_samples, max_recursion_lvls, include_reference,
                include_psd, num_bs_sigs_per_input, include_mod_label, normalize)

        validate_generator = CNNDataGenerator(validate_data_root, batch_size, num_samples, max_recursion_lvls, include_reference,
                include_psd, num_bs_sigs_per_input, include_mod_label, normalize)

        return train_generator, validate_generator

    def __len__(self):
        return int(np.floor(self.num_samples/self.batch_size))

    def __getitem__(self, idx):
        # get the sample indicies that correspond to a batch
        batch_idx = self.sample_idx[(idx*self.batch_size):(idx*self.batch_size)+self.batch_size]

        input, label = self.__data_generation(batch_idx)

        input = np.squeeze(input)

        return input, label
        

    
    def __data_generation(self, idx_list):
        # create batch arrays and reference indexes
        batch_input_shape, rx_idx, ref_idx, psd_idx = self.get_batch_shape_samples()
        input = np.empty(batch_input_shape, dtype=np.complex64)
        label = np.empty(self.get_batch_shape_labels())

        # iterate through batch idx and return the data corresponding to user preferences
        for batch_id, idx in enumerate(idx_list):
            # seperate into dataset index and sample index
            dataset_idx = self.sample_info[idx][0]
            sample_idx = self.sample_info[idx][1]

            # put together the input
            rx = np.array(self.datasets[dataset_idx]['signals/rx_signals'][:self.num_bs_sigs_per_input,sample_idx,:])

            if self.normalize:
                in_max = np.abs(rx).max()
                input[batch_id,:,:,rx_idx] = rx/in_max
            else:
                input[batch_id,:,:,rx_idx] = rx

            if self.include_reference:
                ref = np.array(self.datasets[dataset_idx]['signals/rx_reference'][:self.num_bs_sigs_per_input,sample_idx,:])
   
                if self.normalize:
                    input[batch_id,:,:,ref_idx] = ref/in_max # normalize against rx max to keep levels consitent
                else:
                    input[batch_id,:,:,ref_idx] = ref
            # TODO: add an option to include PSD information as well
            if self.include_psd:
                # normalize data if needed
                psd = np.fft.fftshift(np.fft.fft(self.datasets[dataset_idx]['signals/rx_reference'][:self.num_bs_sigs_per_input,sample_idx,:]))
                if self.normalize:
                    input[batch_id,:,:,psd_idx] = psd/np.abs(psd).max()
                else:
                    input[batch_id,:,:,psd_idx] = psd
                

            # put together the label
            cf = self.datasets[dataset_idx]['labels/center_freq'][sample_idx]
            bw = self.datasets[dataset_idx]['labels/bandwidth'][sample_idx]
            start_idx = self.datasets[dataset_idx]['labels/start_index'][sample_idx]
            end_idx = self.datasets[dataset_idx]['labels/end_index'][sample_idx]
            snr = self.datasets[dataset_idx]['labels/SNR'][sample_idx]

            # normalize labels if requested
            if self.normalize:
                start_idx = start_idx/self.num_samples
                end_idx = end_idx/self.num_samples
                snr = snr/self.snr_scaling

            # in the future make modulation a categorical classifier
            mod = self.datasets[dataset_idx]['labels/modulation'][sample_idx]
            if (mod == 'QPSK'):
                mod = np.array([1,0,0])
            elif (mod == '16QAM'):
                mod = np.array([0,1,0])
            elif (mod == '64QAM'):
                mod = np.array([0,0,1])
            else:
                mod = np.array([0,0,0])
            
            if self.include_mod_label:
                label[batch_id,:] = np.array([mod])
            else:
                label[batch_id,:] = np.array([cf,bw,start_idx,end_idx,snr])

        return input, label


    def get_batch_shape_samples(self):
        # calculate the batch shape
        channels = 1
        channels += 1 if self.include_reference else 0
        channels += 1 if self.include_psd else 0

        sig_len = 2048 #set size of signal length can change in the future

        # generate idexes for the standard rx input, reference input, and psd
        # order by rx idx is always first and psd index is always last if it is included
        rx_idx = 0
        ref_idx = 1 if self.include_reference else None
        if self.include_psd:
            if self.include_reference:
                psd_idx = 2
            else:
                psd_idx = 1
        else:
            psd_idx = None
        
        return (self.batch_size,self.num_bs_sigs_per_input,sig_len,channels), rx_idx, ref_idx, psd_idx

    def get_input_shape(self):
        # calculate the batch shape
        channels = 1
        channels += 1 if self.include_reference else 0
        channels += 1 if self.include_psd else 0

        sig_len = 2048 #set size of signal length can change in the future

        # generate idexes for the standard rx input, reference input, and psd
        # order by rx idx is always first and psd index is always last if it is included
        rx_idx = 0
        ref_idx = 1 if self.include_reference else None
        if self.include_psd:
            if self.include_reference:
                psd_idx = 2
            else:
                psd_idx = 1
        else:
            psd_idx = None
        

        # if any of the dimesnions lengths is 1 take it out and return a smaller shape
        if channels == 1:
            if self.num_bs_sigs_per_input == 1:
                return (sig_len)
            else:
                return (self.num_bs_sigs_per_input,sig_len)
                
        return (self.num_bs_sigs_per_input,sig_len,channels)

    def get_batch_shape_labels(self):
        num_labels = 3 if self.include_mod_label else 5

        return (self.batch_size,num_labels)

    def get_num_sample_labels(self):
        # label is size 3 with modulation since we use categroical inference
        num_labels = 3 if self.include_mod_label else 5
        return num_labels
        
    def get_mod_id(self):
        return {'QPSK': 0, '16QAM': 1, '64QAM': 2}
