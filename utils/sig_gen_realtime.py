"""
A class for generateing signals in real time 
"""
import numpy as np

import os
import h5py

from gnuradio import blocks
from gnuradio import digital
from gnuradio import gr

class sig_gen(gr.top_block):
    def __init__(self, num_samps, sps, samp_rate):
    

        gr.top_block.__init__(self, "Not titled yet")

        ##################################################
        # Variables
        ##################################################
        self.variable_constellation_0 = variable_constellation_0 = digital.constellation_16qam().base()
        self.samp_symb = samp_symb = sps
        self.numb_samps = num_samps
        self.center = center = 0
        self.band_available = band_available = 120e6
        self.samp_rate = samp_rate

        ##################################################
        # Blocks
        ##################################################
        self.digital_constellation_modulator_0 = digital.generic_mod(
            constellation=variable_constellation_0,
            differential=True,
            samples_per_symbol=samp_symb,
            pre_diff_code=True,
            excess_bw=0.05,
            verbose=False,
            log=False)
        self.analog_random_source_x_0 = blocks.vector_source_b(list(map(int, np.random.randint(0, 256, num_samps))), False)
        self.blocks_vector_sink_x_0 = blocks.vector_sink_c(1)
        self.blocks_throttle = blocks.throttle(8,samp_rate)

        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_random_source_x_0, 0), (self.digital_constellation_modulator_0, 0))
        self.connect((self.digital_constellation_modulator_0, 0), (self.blocks_throttle, 0))
        self.connect((self.blocks_throttle,0),(self.blocks_vector_sink_x_0,0))


    def get_variable_constellation_0(self):
        return self.variable_constellation_0

    def set_variable_constellation_0(self, variable_constellation_0):
        self.variable_constellation_0 = variable_constellation_0


    def get_numb_samps(self):
        return self.numb_samps

    def set_numb_samps(self, numb_samps):
        self.numb_samps = numb_samps

    def get_center(self):
        return self.center

    def set_center(self, center):
        self.center = center

    def get_samp_symb(self):
        return self.samp_symb

    def set_samp_symb(self, samp_symb):
        self.samp_symb = samp_symb
        self.digital_constellation_modulator_0 = digital.generic_mod(
            constellation=self.variable_constellation_0,
            differential=True,
            samples_per_symbol=samp_symb,
            pre_diff_code=True,
            excess_bw=0.05,
            verbose=False,
            log=False)

    def get_band_available(self):
        return self.band_available

    def set_band_available(self, band_available):
        self.band_available = band_available

    def get_signals(self, num_sigs):
        for i in range(num_sigs):
            #change the bandwidth and center freq to random values
            self.center = np.random.random_sample()/10.0
            self.samp_rate = samp_rate = np.random.randint(2e6,5e6)
            bandwidth = 2*1/self.samp_symb * self.samp_rate #Compute bandwidth relative to size of bandwidth available.
            
            #run the signal
            self.run()

            sig_data = np.array(self.blocks_vector_sink_x_0.data())[self.numb_samps:2*self.numb_samps]
            yield sig_data, self.center, bandwidth,0

    def get_single_signal(self):
        #change the bandwidth and center freq to random values
        self.center = np.random.random_sample()/10.0
        self.samp_rate = samp_rate = np.random.randint(2e6,5e6)
        bandwidth = 2*1/self.samp_symb * self.samp_rate #Compute bandwidth relative to size of bandwidth available.
        
        #run the signal
        self.run()

        sig_data = np.array(self.blocks_vector_sink_x_0.data())[self.numb_samps:2*self.numb_samps]
        return sig_data, self.center, bandwidth,0

    def __del__(self):
        self.stop()
        self.wait()

def gen_signals(config_params):
    num_signals = config_params['num_signals']
    sig_len = config_params['signal_length']
    for i in range(num_signals):
        #tic = time.time()
        samp_rate = np.random.randint(2e6,5e6)
        sps = 4
        tb = sig_gen(sig_len, sps, samp_rate)   #Initiates a new signal using new sps value

        N = tb.numb_samps 

        tb.run()
        my_data = tb.blocks_vector_sink_x_0.data() 
        my_data = np.asarray(my_data, complex)[-sig_len:] #Output of vector block is a tuple, so convert it to a complex number.
        my_data = my_data/np.max(np.abs(my_data)) # normalize the tx signal to have max of 1 for power

        bandwidth = (2*float(samp_rate))/float(sps)#Compute bandwidth relative to size of bandwidth available.
        #toc = time.time()
        #print(toc-tic)
        return my_data, tb.get_center(), bandwidth, 0