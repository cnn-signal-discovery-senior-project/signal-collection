#!/usr/bin/python3

"""
The prupose of this script is to convert collected SOUNDER files generated with known
generated signal file and convert it to a CNN training dataset.

It does this by scanning a file recusivly until it finds a directory with a hdf5 file in
it. it will then combine all hdf5 files in that directory into a single CNN
training dataset file. it will also preserve the file structure using the out-data parmeter
as its root directory. It will not go beyond 3 recursion levels to find a valid SOUNDER hdf5 
by default. We do this since it follows our data collection methology of saving the mulptle runs
done to capture various gain levels at a single datapoint to a single directory with a location 
tag. So perserving the file structure is esential and since each sampling at a datapoint includes the same
experiment parameters they can easily be placed in the training file for convience.


example input dir

- in_dir
    - run1
        - location1
            - rec1.hdf5
            - rec2.hdf5
            - rec3.hdf5
        - location2
            - rec1.hdf5
            - rec2.hdf5
    - run2
        - location1
            - rec1.hdf5
            - rec2.hdf5
            - rec3.hdf5
            - rec4.hdf5

- out_dir
    - run1
        - location1
            - CNNdataset.hdf5
        - location2
            - CNNdataset.hdf5
    - run2
        - location1
            - CNNdataset.hdf5


The program can also be used to view a frame of data visually using the debug option

JoshuaMiraglia@gmail.com
"""


import sys
import os
sys.path.append(sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'utils', 'IrisUtils')))

from h5py._hl import dataset
import numpy as np
import h5py
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import collections
import time
from subprocess import Popen, PIPE
import glob

from optparse import OptionParser
from channel_analysis import *
import hdf5_lib
from hdf5_lib import *
from fft_power import fft_power

from data_tools import experiment_dataset



def create_CNN_dataset(filename, n_frames, fr_strt, sub_sample, out_data_filename, gen_data_filename=None, num_in_dataset=1):
    #############################################################################
    # Init hdf5_lib for experiment and get usful properties from the metadata
    #############################################################################
    
    hdf5_exp = hdf5_lib(filename, n_frames, fr_strt, sub_sample)
    
    # Get usful properties from the metadata 
    metadata = hdf5_exp.metadata
    frames_to_grab = int(metadata['UL_DATA_FRAME_NUM'])

    fft_size = int(metadata['FFT_SIZE'])
    cp = int(metadata['CP_LEN'])

    prefix_len = int(metadata['PREFIX_LEN'])
    postfix_len = int(metadata['POSTFIX_LEN'])
    z_padding = prefix_len + postfix_len
    samps_per_slot = int(metadata['SLOT_SAMP_LEN'])
    samps_per_slot_no_pad = samps_per_slot - z_padding
    symbol_per_slot = ((samps_per_slot_no_pad) // (cp + fft_size))

    samp_rate = int(metadata['RATE'])
    trans_freq = int(metadata['FREQ'])

    num_cl = int(metadata['CL_NUM'])
    ul_slots = int(metadata['UL_SLOTS'])
    num_bs_ants = int(metadata['BS_ANT_NUM_PER_CELL'])

    # initialize the hdf5 dataset file to save data to
    # if the data file exist use that to initialize the dataset, otherwise create a new one
    if os.path.exists(out_data_filename):
        print("adding to existing datasets is not yet supported")

        out_data = experiment_dataset(sig_len=samps_per_slot,
                                      num_sigs=frames_to_grab,
                                      num_clients=num_cl,
                                      num_elements=num_bs_ants,
                                      sample_rate=samp_rate,
                                      transmit_freq=trans_freq,
                                      file_name=out_data_filename,
                                      in_metadata=metadata,
                                      blank=True)
        out_data.load_from_dataset(out_data_filename)
    else:
        out_data = experiment_dataset(sig_len=samps_per_slot,
                                      num_sigs=frames_to_grab,
                                      num_clients=num_cl,
                                      num_elements=num_bs_ants,
                                      sample_rate=samp_rate,
                                      transmit_freq=trans_freq,
                                      file_name=out_data_filename,
                                      in_metadata=metadata)

    return out_data


"""
Find the first valid frame of a SOUNDER hdf5 file and return the offset index

"""
def get_valid_frame_offset(hdf5_ds, debug=False):
    # define needed parameters for the test
    metadata = hdf5_ds.metadata
    frames_to_grab = int(metadata['UL_DATA_FRAME_NUM'])
    samps_per_slot = int(metadata['SLOT_SAMP_LEN'])
    gain = int(metadata['CL_TX_GAIN_A'][0])

    fr_strt = 0
    n_frames = 25 # the number of frames to look through each cycle
    frame_pwr = []
    frame_count = 0
    while fr_strt < frames_to_grab:
        # grab the next batch of data from the hdf5 file
        if fr_strt+n_frames > frames_to_grab:
            frm_end = frames_to_grab
        else:
            frm_end = fr_strt+n_frames

        hdf5_ds.n_frm_st = fr_strt
        hdf5_ds.n_frm_end = frm_end
        hdf5_ds.get_data()

        uplink_samples = hdf5_ds.uplink_samples

        # uplink sample dimensions
        # ( #frames, #cells, #ul_uplink_slot, #bs nodes or # bs ant, #samps per frame * 2 for IQ )
        num_frames = uplink_samples.shape[0]
        num_cells = uplink_samples.shape[1]

        # cycle through antennas and clients to grab rx signals from each
        # cycle through each signal and proccess it
        bs_ref = 0
        threshold = [-15] # dBm
        for frameIdx in range(num_frames): 
            frame_count += 1
            for cellIdx in range(num_cells):
                # convert ul samples to IQ format
                I = uplink_samples[frameIdx, cellIdx, 0, bs_ref, 0:samps_per_slot * 2:2] / 2 ** 15
                Q = uplink_samples[frameIdx, cellIdx, 0, bs_ref, 1:samps_per_slot * 2:2] / 2 ** 15
                IQul = I + (Q * 1j)

                # compute the power of the uplink slot for this frame, won't be exact as signal doesn't span the entire
                # freuency but it should be close enough
                rms = np.sqrt(np.mean(IQul[:] * np.conj(IQul[:])))
                td_pwr_lin = np.real(rms) ** 2
                td_pwr_dbm_s = 10 * np.log10(td_pwr_lin / 1e-3)

                frame_pwr.append(td_pwr_dbm_s)

                # check if valid frame or not
                # there could be a better way to do this but foor now look for a power higher than -30dB
                # this works nice for the anechoic chmber data but should propbly use edge detection for outdoor data
                # TODO: maybe us convolution to find a spike for an increase in power over a single frame
                if td_pwr_dbm_s > threshold[-1]: # use the most recent threshold to check against
                    if debug:
                        peak = np.argmax(frame_pwr)
                        plt.plot(frame_pwr, label="Frame Pwr")
                        plt.plot(threshold, label="valid frame thresh")
                        plt.title("power of uplink data per frame, gain: {}, frame: {}".format(gain, peak))
                        plt.xlabel("Frame")
                        plt.ylabel("Power (dBm)")
                        plt.legend(fontsize=10, loc='upper right', shadow=True, ncol=2)
                        plt.show()
                    # power is high enough exit with the current frame index as the offset
                    return frame_count

                # update the trheshold value
                new_thresh = (threshold[-1] + sum(frame_pwr) + .75*len(frame_pwr))/(len(threshold)+1)
                #print("{}, {}, {}, {}".format(new_thresh, td_pwr_dbm_s, sum(threshold), len(threshold)))
                threshold.append(new_thresh)

        # incremen the frames to process
        fr_strt = frm_end

        if debug:
            peak = np.argmax(frame_pwr)
            plt.plot(frame_pwr, label="Frame Pwr")
            plt.plot(threshold, label="valid frame thresh")
            plt.title("power of uplink data per frame, gain: {}, frame: {}".format(gain, peak))
            plt.xlabel("Frame")
            plt.ylabel("Power (dBm)")
            plt.legend(fontsize=10, loc='upper right', shadow=True, ncol=2)
            plt.show()

    return -1
    

def process_hdf5_file(filename, n_frames, fr_strt, sub_sample, out_data, gen_data_filename=None, debug=False, debug_frames=10):
    #############################################################################
    # Init hdf5_lib for experiment and get usful properties from the metadata
    #############################################################################
    
    hdf5_exp = hdf5_lib(filename, n_frames, fr_strt, sub_sample)
    
    # Get usful properties from the metadata 
    metadata = hdf5_exp.metadata
    frames_to_grab = int(metadata['UL_DATA_FRAME_NUM'])

    fft_size = int(metadata['FFT_SIZE'])
    cp = int(metadata['CP_LEN'])

    prefix_len = int(metadata['PREFIX_LEN'])
    postfix_len = int(metadata['POSTFIX_LEN'])
    z_padding = prefix_len + postfix_len
    samps_per_slot = int(metadata['SLOT_SAMP_LEN'])
    samps_per_slot_no_pad = samps_per_slot - z_padding
    symbol_per_slot = ((samps_per_slot_no_pad) // (cp + fft_size))

    samp_rate = int(metadata['RATE'])
    trans_freq = int(metadata['FREQ'])

    num_cl = int(metadata['CL_NUM'])
    ul_slots = int(metadata['UL_SLOTS'])
    
    num_bs_ants = int(metadata['BS_ANT_NUM_PER_CELL'])


    # define a 16 bit complex number for data signals
    dt_c = np.dtype({"names":['re','im'], 
                    "formats": ['<f2','<f2']})

    dt_c_tmp = np.dtype({"names":['re','im'], 
                    "formats": ['<f4','<f4']})


    ##############################################
    # open sent signal data to compare signals
    ###############################################
    if  gen_data_filename is not  None:
        hdf5_sent_data =h5py.File(gen_data_filename, 'r')
        tx_data = hdf5_sent_data['generated_data/dataset'][:10000,:].astype(np.complex64)
        start_time_labels = hdf5_sent_data['generated_data/starts']
        stop_time_labels = hdf5_sent_data['generated_data/stops']
        center_freq_labels = hdf5_sent_data['generated_data/centers']
        bandwidth_labels = hdf5_sent_data['generated_data/bandwidths']
        modulations = hdf5_sent_data['generated_data/modulations']

    #############################################################################
    # First few frames of a SOUNDER file are trash so fine the first valid frame
    #############################################################################
    frame_offset = get_valid_frame_offset(hdf5_exp, debug=debug)

    # if the requested start frame is smaller than the offset you are starting on an invalid frame
    # so change it to the grame offset
    if frame_offset > fr_strt:
        fr_strt = frame_offset

    ##########################################################################################
    # cycle through all uplink data frames for the file and save them to a traingin dataset
    ##########################################################################################
    while fr_strt < frames_to_grab:
        # calculate how many frames too get in the next cycle
        
        if fr_strt+n_frames > frames_to_grab:
            frm_end = frames_to_grab
        else:
            frm_end = fr_strt+n_frames
        
        # get data for next cycle
        hdf5_exp.n_frm_st = fr_strt
        hdf5_exp.n_frm_end = frm_end
        hdf5_exp.get_data() # updates class paramters with new samples between n_frm_st and n_frm_end

        pilot_samples = hdf5_exp.pilot_samples
        uplink_samples = hdf5_exp.uplink_samples
        noise_samples = hdf5_exp.noise_samples
            
        # Check which data we have available
        pilots_avail = len(pilot_samples) > 0
        ul_data_avail = len(uplink_samples) > 0
        noise_avail = len(noise_samples) > 0

        if not (pilots_avail and ul_data_avail):
                print('no data found skipping frames {}-{}'.format(fr_strt,frm_end))
                fr_strt = frm_end
                continue
        else:
            print('processing frames {}-{}'.format(fr_strt, frm_end))


        if not noise_avail:
            noise_samples = np.ones((pilot_samples.shape))*.0001


        ######################################################################
        # Process experiment files and extract the usful data for training
        ######################################################################

        # pilot_samples dimensions:
        # ( #frames, #cells, #pilot subframes or cl ant sending pilots, #bs nodes or # bs ant, #samps per frame * 2 for IQ )
        num_frames = pilot_samples.shape[0]
        num_cells = pilot_samples.shape[1]
        num_bs_ants = pilot_samples.shape[3]


        computer_SNR = True
        if computer_SNR:
            # allocate space for snr values for all uplink samples
            snr = np.empty_like(uplink_samples[:, :, :, :, 0], dtype=float)
            

        # initialize arrays for storing debug information if using
        frames_to_plot = 0
        if debug:
            frames_to_plot = debug_frames
            sig_fig = plt.figure()
            frame_IQul = np.empty((frames_to_plot,num_cells,ul_slots,num_bs_ants,int(samps_per_slot)), dtype=np.complex64)
            frame_IQn = np.empty((frames_to_plot,num_cells,ul_slots,num_bs_ants,int(samps_per_slot)), dtype=np.complex64)



        # cycle through each signal and proccess it
        for frameIdx in range(num_frames):    # Frame
            frame_rx_data = np.empty((num_bs_ants,ul_slots,int(samps_per_slot)), dtype=np.complex64)

            #grab relavent labels from the generated signal file fopr this frame
            gen_frame_index = fr_strt+frameIdx-frame_offset
            if gen_frame_index >= tx_data.shape[0]:
                #exceeded maximum number of tx signals break the loop
                break
            center_freq = center_freq_labels[gen_frame_index]
            bandwidth=bandwidth_labels[gen_frame_index]
            start_ind=start_time_labels[gen_frame_index]
            stop_ind=stop_time_labels[gen_frame_index]
            mod=modulations[gen_frame_index]

            # cycle through antennas and clients to grab rx signals from each
            for cellIdx in range(num_cells):
                for clIdx in range(ul_slots):  # iterate through clients, each client should transmit over its own uplink slot
                    for bsAntIdx in range(num_bs_ants):  # BS ANT
                        # convert ul samples to IQ format
                        I = uplink_samples[frameIdx, cellIdx, clIdx, bsAntIdx, 0:samps_per_slot * 2:2] / 2 ** 15
                        Q = uplink_samples[frameIdx, cellIdx, clIdx, bsAntIdx, 1:samps_per_slot * 2:2] / 2 ** 15
                        IQul = I + (Q * 1j)
                        frame_rx_data[bsAntIdx,clIdx,:] = IQul

                        if debug:
                            frame_IQul[frameIdx,cellIdx, clIdx, bsAntIdx,:]= IQul

                        # conver the ul IQ samples to time domain signals
                        samp_time = 1/samp_rate
                        time_per_slot = len(IQul)*samp_time
                        t = np.linspace(0, time_per_slot, num=len(IQul))
                        ul_sig = np.real(np.multiply(IQul,np.exp(1j*trans_freq*t)))

                        # Compute SNR
                        # get IQ values for the noise_samples
                        In = noise_samples[frameIdx, cellIdx, 0, bsAntIdx, 0:samps_per_slot * 2:2] / 2 ** 15
                        Qn = noise_samples[frameIdx, cellIdx, 0, bsAntIdx, 1:samps_per_slot * 2:2] / 2 ** 15
                        IQn = np.array(In + (Qn * 1j), dtype=np.complex64)

                        if debug:
                            frame_IQn[frameIdx,cellIdx, clIdx, bsAntIdx,:] = IQn


                        #hdf5_sent_data =h5py.File(gen_data_filename, 'r')
                        #rms = np.empty((30000,))
                        #for i in range(30000):
                        #    ul_sent_data = hdf5_sent_data['generated_data/dataset'][i,:]
                        #    rms[i] = ((np.abs(ul_sent_data)-np.abs(IQul))**2).sum()

                        SNR = 0
                        if computer_SNR:
                            # Compute Noise Power (Time Domain)
                            rms = np.sqrt(np.mean(IQn * np.conj(IQn)))
                            td_pwr_lin = np.real(rms) ** 2
                            td_pwr_dbm_n = 10 * np.log10(td_pwr_lin / 1e-3)

                            # Computer Signal Power (Time Domain)
                            rms = np.sqrt(np.mean(IQul[int(start_ind):int(stop_ind)] * np.conj(IQul[int(start_ind):int(stop_ind)])))
                            td_pwr_lin = np.real(rms) ** 2
                            td_pwr_dbm_s = 10 * np.log10(td_pwr_lin / 1e-3)

                            # SNR
                            SNR = td_pwr_dbm_s - td_pwr_dbm_n
                            snr[frameIdx, cellIdx, clIdx, bsAntIdx] = td_pwr_dbm_s - td_pwr_dbm_n


            # save frame to dataset
            out_data.save_frame(rx_sigs=frame_rx_data,
                                tx_sig=tx_data[gen_frame_index],
                                ref_sig=IQn,
                                center_freq=center_freq,
                                bandwidth=bandwidth,
                                start_ind=start_ind,
                                end_ind=stop_ind,
                                SNR=SNR,
                                mod=mod)


            
            if debug:
                ax1 = sig_fig.add_subplot(2,10,(frameIdx)+1)
                ax2 = sig_fig.add_subplot(2,10,(frameIdx)+11)
                ax1.plot(IQul)
                ax1.set_title('rx_frame {}'.format(frameIdx))
                ax2.plot(tx_data[gen_frame_index])
                ax2.set_title('tx_frame {}'.format(gen_frame_index))

            if debug and frameIdx == frames_to_plot-1:
                # lets do some plotting to debug the data files
                print('plotting a few frames of data')
                ref_ant = 0

                phase_relathions = False
                if phase_relathions:
                    # only plot the phase relathion of an antenna
                    plot_phase_relathion(frame_IQul, metadata)
                    return
                

                for plot_frameIdx in range(frames_to_plot):
                    fig = plt.figure(figsize=(20, 20), dpi=120)
                    fig.subplots_adjust(hspace=.5, top=.9, bottom=.1, wspace=1)
                    gs = gridspec.GridSpec(ncols=8, nrows=4)

                    ###############################################
                    # specify plots
                    ###############################################
                    ax_tx_sig = fig.add_subplot(gs[0, 0:4])
                    ax_tx_sig.set_title('TX Signal', fontsize=10)
                    ax_tx_sig.text(0.5, 1, '|', ha="center")
                    ax_tx_sig.set_ylabel('Magnitude')
                    ax_tx_sig.set_xlabel('Sample index')
                    ax_tx_sig.set_ylim(-1.00, 1.00)
                    
                    ax_tx_psd = fig.add_subplot(gs[0, 4:8])
                    ax_tx_psd.set_title('TX Signal PSD', fontsize=10)
                    ax_tx_psd.text(0.5, 1, '|', ha="center")
                    ax_tx_psd.set_ylabel('Magnitude(dB)')
                    ax_tx_psd.set_xlabel('Frequency')

                    ax_rx_frame = fig.add_subplot(gs[1, 0:8])
                    ax_rx_frame.set_title('RX signal of frame', fontsize=10)
                    ax_rx_frame.text(0.5, 1, '|', ha="center")
                    ax_rx_frame.set_ylabel('Magnitude')
                    ax_rx_frame.set_xlabel('Sample index')

                    ax_cl1_rx = fig.add_subplot(gs[2, 0:4])
                    ax_cl1_rx.set_title('RX signal Client 1', fontsize=10)
                    ax_cl1_rx.text(0.5, 1, '|', ha="center")
                    ax_cl1_rx.set_ylabel('Magnitude')
                    ax_cl1_rx.set_xlabel('Sample index')

                    ax_cl2_rx = fig.add_subplot(gs[2, 4:8])
                    ax_cl2_rx.set_title('RX signal Client 2', fontsize=10)
                    ax_cl2_rx.text(0.5, 1, '|', ha="center")
                    ax_cl2_rx.set_ylabel('Magnitude')
                    ax_cl2_rx.set_xlabel('Sample index')

                    ax_cl1_psd = fig.add_subplot(gs[3, 0:4])
                    ax_cl1_psd.set_title('PSD UL Client 1', fontsize=10)
                    ax_cl1_psd.text(0.5, 1, '|', ha="center")
                    ax_cl1_psd.set_ylabel('Magnitude')
                    ax_cl1_psd.set_xlabel('Frequency')

                    ax_cl2_psd = fig.add_subplot(gs[3, 4:8])
                    ax_cl2_psd.set_title('PSD UL Client 2', fontsize=10)
                    ax_cl2_psd.text(0.5, 1, '|', ha="center")
                    ax_cl2_psd.set_ylabel('Magnitude')
                    ax_cl2_psd.set_xlabel('Frequency')

                    ########################################
                    # populate plots
                    ########################################

                    # get signals used in this frame
                    #tx_frame_inx = len(tx_data) - (12+plot_frameIdx)
                    tx_frame_inx = fr_strt + plot_frameIdx - frame_offset
                    ul_sent_data = tx_data[tx_frame_inx]
                    IQul_cl1 = frame_IQul[plot_frameIdx,0,0,ref_ant,:]
                    #IQul_cl1 = frame_IQul[tx_frame_inx,0,0,plot_frameIdx*2,:]
                    if ul_slots == 2:
                        #IQul_cl2 = frame_IQul[tx_frame_inx,0,1,plot_frameIdx*2+1,:]
                        IQul_cl2 = frame_IQul[plot_frameIdx,0,1,ref_ant,:]
                    IQn = frame_IQn[plot_frameIdx,0,0,ref_ant,:]

                    # plot sent signal and PSD oof sent signal
                    line = np.linspace(np.min(ul_sent_data), np.max(ul_sent_data), num=100)
                    ax_tx_sig.plot(range(len(ul_sent_data)), np.real(ul_sent_data))
                    ax_tx_sig.plot(range(len(ul_sent_data)), np.imag(ul_sent_data))
                    ax_tx_sig.plot(start_time_labels[tx_frame_inx] * np.ones(100), line, '--g', label='Start Time')  # markers
                    ax_tx_sig.plot(stop_time_labels[tx_frame_inx] * np.ones(100), line, '--r', label='Stop Time')  # markers
                    ax_tx_sig.legend(fontsize=10, loc='upper right', shadow=True, ncol=2)
                    

                    freq, psd, nf, peak = fft_power(ul_sent_data, samp_rate)
                    ax_tx_psd.plot(freq, psd)

                    # reconstruct the frame data without guides
                    rx_data = []
                    #add pilots
                    for clIdx in range(num_cl):
                        # get IQ samples of the pilot
                        I = pilot_samples[plot_frameIdx, cellIdx, clIdx, ref_ant, 0:samps_per_slot * 2:2] / 2 ** 15
                        Q = pilot_samples[plot_frameIdx, cellIdx, clIdx, ref_ant, 1:samps_per_slot * 2:2] / 2 ** 15
                        IQpilot = I + (Q * 1j)
                        rx_data.extend(IQpilot)
                    rx_data.extend(IQul_cl1)
                    if ul_slots == 2:
                        rx_data.extend(IQul_cl2)
                    rx_data.extend(IQn)

                    ax_rx_frame.plot(range(len(rx_data)), np.real(rx_data))
                    line = np.linspace(np.min(rx_data), np.max(rx_data), num=100)
                    pilot1_start = prefix_len
                    pilot2_start = samps_per_slot + prefix_len
                    ul1_start = (2*samps_per_slot)
                    ul2_start = (3*samps_per_slot)
                    noise_start = (4*samps_per_slot) + prefix_len
                    ax_rx_frame.plot(pilot1_start * np.ones(100), line, '--k', label='Pilot 1 Start')  # markers
                    ax_rx_frame.plot(pilot2_start * np.ones(100), line, '--b', label='Pilot 2 Start')  # markers
                    ax_rx_frame.plot(ul1_start * np.ones(100), line, '--g', label='client 1 Start')  # markers
                    ax_rx_frame.plot(ul2_start * np.ones(100), line, '--m', label='client 2 Start')  # markers
                    ax_rx_frame.plot(noise_start * np.ones(100), line, '--r', label='Noise Samp Start')  # markers
                    ax_rx_frame.legend(fontsize=8, loc='lower right', shadow=True, ncol=2)

                    # plot client ul data slots
                    line = np.linspace(np.min(IQul_cl1), np.max(IQul_cl1), num=100)
                    ax_cl1_rx.plot(range(len(IQul_cl1)), np.real(IQul_cl1))
                    ax_cl1_rx.plot(range(len(IQul_cl1)), np.imag(IQul_cl1))
                    ax_cl1_rx.plot(start_time_labels[tx_frame_inx] * np.ones(100), line, '--g', label='Start Time')  # markers
                    ax_cl1_rx.plot(stop_time_labels[tx_frame_inx] * np.ones(100), line, '--r', label='Stop Time')  # markers
                    ax_cl1_rx.legend(fontsize=10, loc='upper right', shadow=True, ncol=2)

                    if ul_slots == 2:
                        line = np.linspace(np.min(IQul_cl2), np.max(IQul_cl2), num=100)
                        ax_cl2_rx.plot(range(len(IQul_cl2)), np.real(IQul_cl2))
                        ax_cl2_rx.plot(range(len(IQul_cl2)), np.imag(IQul_cl2))
                        ax_cl2_rx.plot(start_time_labels[tx_frame_inx] * np.ones(100), line, '--g', label='Start Time')  # markers
                        ax_cl2_rx.plot(stop_time_labels[tx_frame_inx] * np.ones(100), line, '--r', label='Stop Time')  # markers
                        ax_cl2_rx.legend(fontsize=10, loc='upper right', shadow=True, ncol=2)
                    
                    # plot PSD
                    freq, psd, nf, peak = fft_power(IQul_cl1, samp_rate)
                    ax_cl1_psd.plot(freq, psd)

                    if ul_slots == 2:
                        freq, psd, nf, peak = fft_power(IQul_cl2, samp_rate)
                        ax_cl2_psd.plot(freq, psd)

                #####################################################################
                # debug code for plotting phase relathionships accross an array
                #####################################################################
                plot_phase_relathion(frame_IQul, metadata)
                return
                  
                            
        # incremen the frames to process
        fr_strt = frm_end
    return

def plot_phase_relathion(frame_IQul, metadata):
    fig2 = plt.figure(figsize=(20, 20), dpi=120)
    fig3 = plt.figure(figsize=(20, 20), dpi=120)

    bs_sdr_id = metadata['BS_SDR_ID']
    num_cl = int(metadata['CL_NUM'])
    ul_slots = int(metadata['UL_SLOTS'])
    num_bs_ants = 24 # int(metadata['BS_SDR_NUM_PER_CELL'])
    samps_per_slot = int(metadata['SLOT_SAMP_LEN'])

    ref_frame = 0
    ref_ant = 0
    frame_numer = frame_IQul.shape[0]
    phase_peaks = np.empty((frame_numer,num_bs_ants), dtype=float)

    # correlation focus
    ref_bs = 0

    for frame_idx in range(frame_numer):
        
        cors = np.empty((num_bs_ants, samps_per_slot), dtype=complex)
        for bs_idx in range(num_bs_ants):

            

            # grab the data for transmition client for each base station element and get its phase
            IQul_cl1 = frame_IQul[frame_idx,0,0,bs_idx,:]
            IQul_ref = frame_IQul[frame_idx,0,1,bs_idx,:]


            phase = np.unwrap(np.angle(IQul_cl1) - np.angle(IQul_ref))

            # grab the line of best fit
            line = np.polyfit(range(len(phase)), phase, deg=1)

            #cor = np.correlate(IQpilot_frame[i-1,:], IQpilot_frame[i-2,:], 'same')
            cor = np.correlate(IQul_ref, IQul_cl1, 'same')
            cors[bs_idx,:] = cor
            peak = np.argmax(np.abs(cor))
            phase_peaks[frame_idx,bs_idx] = peak


        # plot the first 24 frames of phases as it gets overwhelming after that
        if frame_idx < 24:
            #setup the plot
            ax = fig2.add_subplot(6,4,frame_idx+1)
            ax.set_title('BS antenna {}, id: {}'.format(frame_idx,bs_sdr_id[frame_idx-1]), fontsize=10)
            ax.text(0.5, 1, '|', ha="center")
            ax.set_title('frame {}, id: {}, peak: {}'.format(frame_idx,bs_sdr_id[bs_idx], phase_peaks[frame_idx,ref_bs]), fontsize=10)
            ax.plot(np.abs(cors[ref_bs,:]))

    # calculate the stats of phase relathions and plot 
    means = np.empty((num_bs_ants,), dtype=float)
    stds = np.empty((num_bs_ants,), dtype=float)
    for i in range(num_bs_ants):
        # claculate the stats for phase relathions with ref antenna
        bs_phases = phase_peaks[:,i]
        mean = np.mean(bs_phases)
        std = np.std(bs_phases)

        means[i] = mean
        stds[i] = std

    #setup the plot
    ax = fig3.add_subplot(1,1,1)
    ax.text(0.5, 1, '|', ha="center")
    ax.set_title('BS {}, id: {}, peak: {}'.format(i,bs_sdr_id[i-1],peak), fontsize=10)
    # plot
    ax.errorbar(range(num_bs_ants), means, stds, linestyle='None', marker='^')

    plt.show(block=True)

def main():
    ###############################################
    # Parse optional parameters
    ##################################################
    parser = OptionParser()
    parser.add_option("--in-data-root", type="string", dest="in_data_root", help="The filename or directory for the stored SOUNDER dataset/s to process", default=".\\")
    parser.add_option("--out-data-root", type="string", dest="out_data_root", help="The filename or directory for the stored CNN dataset", default="F:\\POWDER\\")
    parser.add_option("--n-frames", type="int", dest="n_frames", help="Number of frames to save each cycle, decrease if having memory issues", default=100)
    parser.add_option("--sub-sample", type="int", dest="sub_sample", help="Sub sample rate", default=1)
    # the next option for start frame has a default of 13 as the first 12 frames are typicllly trash
    parser.add_option("--frame-start", type="int", dest="fr_strt", help="Starting frame. Must have set n_frames first and make sure fr_strt is within boundaries ", default=0) #17
    parser.add_option("--gen-data", type="string", dest="gen_data_file", help="The filename of the generated data hdf5 file", default="F:\\POWDER\\gen_data\\2048\\Final_2048_4SPS.h5")
    parser.add_option("--debug", dest="debug", help="Debug mode where several frames of data are plotted", default=False)
    parser.add_option("--single-file", dest="single_file", help="process a single file, specify file instead of directory for parametesr", default=False)
    parser.add_option("--max-recurs-lvl", type="int", dest="max_recurs_lvl", help="the max recursive levels the program will search for hdf5 files", default=3)
    
    (options, args) = parser.parse_args()

    in_data_root  = options.in_data_root
    out_data_root = options.out_data_root
    n_frames = options.n_frames
    fr_strt = options.fr_strt
    sub_sample = options.sub_sample
    gen_data_file = options.gen_data_file
    debug = options.debug
    single_file = options.single_file
    max_recursion_lvls = options.max_recurs_lvl

    # get direcotry/file name to process
    filename = in_data_root
    

    if single_file:
        # checks for propery inputs
        if not os.path.isfile(in_data_root):
            print("invalid input file {}, need to give a file as the in-data-root parameter if proscessing a single file".format(out_data_root))


        ##################################################################################
        # process the single file to process and store training dataset
        ##################################################################################
        print('processing hdf5 file {}'.format(filename))
        #TODO: determine a way to figure out how many garbage frames are produced
        #TODO: process all datafiles in a directory
        dataset = create_CNN_dataset(filename, n_frames, fr_strt, sub_sample, out_data_root, gen_data_file)
        process_hdf5_file(filename, n_frames, fr_strt, sub_sample, dataset, gen_data_file, debug=debug)
        try:
            print('processing hdf5 file in here')
            # process_hdf5_file(filename, n_frames, fr_strt, sub_sample, out_data_file, gen_data_file)
        except:
            print('{} cannot be found or is a bad experiment file moving on to the next file'.format(filename))


    else:
        # checks for propery inputs
        if not os.path.isdir(in_data_root):
            print("invalid input directory {}, need to give a directory as the in-data-root parameter to perform search".format(out_data_root))

        ##################################################################################
        # Cycle through directories or single file to process and store training dataset
        ##################################################################################
        for root, dirs, files in os.walk(filename, topdown=True):
            # see if there is and hdf5 files in the current dir
            hdf_found = False

            # setup directory path that mirros the searched one in the output area
            rel_path = os.path.relpath(root, start=in_data_root) # get the relative path in the search dirs relative to input folder
            out_dir = os.path.join(out_data_root, rel_path)
            out_file = os.path.join(out_dir, "CNN_dataset.hdf5")# the name of the output file for all SOUNDER hdf5s in a directory

            for f in files:
                if f.endswith(".hdf5"):
                    cur_file = os.path.join(root, f)
                    if not hdf_found: # this should run the first time a hdf5 file is scanned, so create the dataset for this folder
                        # create the directory if it doesn't already exist
                        if not os.path.exists(out_dir):
                            os.makedirs(out_dir)

                        dataset = create_CNN_dataset(cur_file, n_frames, fr_strt, sub_sample, out_file, gen_data_file)
                        # process_hdf5_file(cur_file, n_frames, fr_strt, sub_sample, dataset, gen_data_file, debug=debug)
                        try:
                            print('processing hdf5 file in here')
                            process_hdf5_file(cur_file, n_frames, fr_strt, sub_sample, dataset, gen_data_file, debug=debug)
                        except:
                            print('{} cannot be found or is a bad experiment file moving on to the next file'.format(cur_file))
                        hdf_found = True

                    else:
                        try:
                            print('processing hdf5 file in here')
                            process_hdf5_file(cur_file, n_frames, fr_strt, sub_sample, dataset, gen_data_file, debug=debug)
                        except:
                            print('{} cannot be found or is a bad experiment file moving on to the next file'.format(cur_file))

            
            # enforce max depth
            if root.count(os.sep) - in_data_root.count(os.sep) == max_recursion_lvls:
                del dirs[:] # if dirs is removed then os.walk wont look to go into those folders 

        dataset.close()
        

if __name__ == '__main__':
    scrpt_strt = time.time()
    main()
    scrpt_end = time.time()
    print(">>>> Script Duration: time: %f \n" % ( scrpt_end - scrpt_strt) )

