"""
signal_processing.py

RENEW OPEN SOURCE LICENSE Copyright © 2018-2019. All rights reserved.
"""
import numpy as np
import matplotlib.pyplot as plt
from numpy.core.fromnumeric import size


import scipy.signal
#from peakdet import *


"""
    given a repeating signal that isn't truly periodic due to phase shifting 
    return a single repeat of that signal i.e. a single frame of the signal

    Should be used when you have a noisy signal capture of multiple instances of 
    tx signal that are not periodic and you only want one repeat of the signal. 
    Right now only works best with low noise signals but could use work in the future to 
    make it more rubust.

    INPUTS:
        in_sig (np.array): signal containing the repeats of a given frame signal
        frame_sig (np.array): the clean signal of a given frame
        frame_len (int): length of a single repeat of the signle i.e. the frame length
    OUTPUTS:
        (np.array) the first instance of the frame_sig in the input signal     
"""
def cap_signal_frame(in_sig, frame_len):
    conv_sig = np.array([-1,-1,-1,1,1,1])
    #conv_sig = np.array([-1,1])
    dicontinuities = np.convolve(conv_sig, in_sig)

    #cut off convolve edges which are too sharp to aboid false positives
    dicontinuities = dicontinuities[len(conv_sig):len(dicontinuities)-len(conv_sig)]

    # capture a disconinuity by finding the largest spike in the magnitude
    disc_ind = (int) (np.argmax(np.abs(dicontinuities)) + np.round(len(conv_sig)/2)) # add half the conv signal to center the discoontinuity

    # using the dicontinutiy as a reference point capture a single frame of the signal using discontinuity as the edge
    frame_sig = np.array([0]*frame_len, np.complex)
    if disc_ind >= np.round(len(dicontinuities)/2):
        # cature frame with discontinuity as the ending point
        frame_sig[:] = in_sig[disc_ind-frame_len:disc_ind]
    else:
        # cature frame with discontinuity as the starting point
        frame_sig[:] = in_sig[disc_ind+1:disc_ind+frame_len+1]


    return frame_sig


def sim_sig_txrx(tx_sig):
    signal_size = len(tx_sig)

    # simulated a phase shift of the signal for the rx reciever and capture 2 copies of tx sig
    phase_ind = np.random.randint(25,signal_size-25)
    rx_sig = np.array([0]*signal_size, np.complex)

    rx_sig[0:signal_size-phase_ind] = tx_sig[phase_ind:]
    rx_sig[signal_size-phase_ind:signal_size] = tx_sig[:phase_ind]
    
    #rx_sig = np.array([0]*signal_size*2, np.complex)

    #rx_sig[0:signal_size-phase_ind] = tx_sig[phase_ind:]
    #rx_sig[signal_size-phase_ind:(2*signal_size)-phase_ind] = tx_sig
    #rx_sig[(2*signal_size)-phase_ind:] = tx_sig[:phase_ind]
    
    # add noise
    rx_sig += np.random.normal(0,.05, size=(len(rx_sig),2)).view(np.complex)[:,0]

    return rx_sig



"""
 INPUT:
        samples     - Numpy array of complex samples
        samp_rate   - Sampling rate
        num_bins    - Take FFTs of this size and average their bins
        peak        - Maximum value of a sample (floats are usually 1.0)
        scaling     - Scaling type. 'density' for power spectrum density
                      (units: V**2/Hz) or 'spectrum' for power spectum
                      (units: V**2)
        peak_thresh - detect peak 'peak_thresh' dBs above the noise floor
    OUTPUT:
        freq        - Frequency index array
        sig_psd     - Array of FFT power bins
        nf          - Noise Floor (dB)
        peaks_found - List of tuples with tone freq and corresponding power
"""
def fft_power(samples, samp_rate, num_bins=None, peak=1.0, scaling='density', peak_thresh=10):

    if num_bins is None:
        num_iters = 1
        num_bins = len(samples)     # use all samples
    else:
        num_iters = len(samples) // num_bins
        num_bins = num_bins

    # Create a list of bins
    bin_list = list()

    for iter in range(num_iters):

        # Current samples
        samps = samples[iter*num_bins:(iter+1)*num_bins]

        # Length of sample vector
        L = len(samps)

        # Generate window... Default to Hann window
        window = scipy.signal.hann(L)

        # Apply window to signal in time domain
        windowed_signal = np.multiply(window, samps/peak)

        # FFT
        sig_fft = np.fft.fft(windowed_signal)
        sig_psd = np.multiply(sig_fft, np.conjugate(sig_fft))               # abs(sig_fft)**2
        # sig_psd = sig_psd[0: L // 2 + 1]                                  # half of PS

        # Scaling factors
        if scaling == 'density':
            s_val = np.sum(window ** 2)
            sig_psd = sig_psd * (1 / (s_val * samp_rate))                   # multiply by 2 if consider only half of PS
        elif scaling == 'spectrum':
            s_val = window.sum() ** 2
            sig_psd = sig_psd * (1 / s_val)                   # multiply by 2 if consider only half of PS
        else:
            raise ValueError('Unknown scaling. Options: density/spectrum')

        # Clip nulls
        sig_psd = np.maximum(sig_psd, 1e-20)
        # Log scale
        sig_psd = 10 * np.log10(sig_psd)

        # freq index
        freq = np.arange((-samp_rate // 2), (samp_rate // 2), samp_rate/L)  # [-Fs / 2: samp_rate/L: Fs / 2]

        # Reorder bins
        idx = np.argsort(np.fft.fftfreq(L))
        sig_psd = sig_psd[idx]

        # Add bins to Bin List
        bin_list.append(np.exp(sig_psd))

    # Re-assign varialbe if num_bins argument is None
    if num_iters != 1:
        avg_bin_log = np.log(sum(bin_list) / num_bins)
        sig_psd = avg_bin_log

    # Get estimate of noise floor
    nf = np.mean(np.real(sig_psd))

    # Find Peaks
    peaks_found = list()
    # local max/min
    #maxMat, _ = peakdet(sig_psd, 20)
    #
    # Remove peaks below nf
    #for idx, val in maxMat:
    #    if val < nf + peak_thresh:
    #        continue
    #    freq_loc = (samp_rate * idx) / len(sig_psd) - samp_rate / 2
    #    peaks_found.append((np.real(freq_loc), np.real(val)))

    return freq, sig_psd, nf, peaks_found


def one_frame_test():
    Ts = 1/5e6 # the time steps given a certain sampling rate
    freq = 20e3
    signal_size = 512
    s_time_vals = np.array(np.arange(0, signal_size)).transpose()*Ts
    data = np.exp(s_time_vals*1j*2*np.pi*freq).astype(np.complex)

    rec_sig = sim_sig_txrx()

    rec_sig_sing_frame = cap_signal_frame(rec_sig, data, len(data))


    fig = plt.figure(figsize=(30, 8), dpi=120)
    fig.subplots_adjust(hspace=.5, top=.85)
    ax1 = fig.add_subplot(2, 2, 1)
    ax1.grid(True)
    ax1.set_ylabel('Signal (units)')
    ax1.set_xlabel('Sample index')
    ax1.plot(range(len(data)), np.real((data)), label='ChA real cl')
    ax1.plot(range(len(data)), np.imag((data)), label='ChA imag cl')
    ax1.set_ylim(-1, 1)
    ax1.legend(fontsize=10)

    ax2 = fig.add_subplot(2, 2, 2)
    ax2.grid(True)
    ax2.set_ylabel('Signal (units)')
    ax2.set_xlabel('Sample index')
    ax2.plot(range(len(rec_sig)), np.real((rec_sig)), label='ChA Real bs')
    ax2.plot(range(len(rec_sig)), np.imag((rec_sig)), label='ChA Imag bs')
    ax2.plot(range(len(rec_sig)), np.abs((rec_sig)), label='ChA mag')
    ax2.legend(fontsize=10)

    ax3 = fig.add_subplot(2, 2, 3)
    ax3.grid(True)
    ax3.set_ylabel('Signal (units)')
    ax3.set_xlabel('Sample index')
    ax3.plot(range(len(rec_sig_sing_frame)), np.real((rec_sig_sing_frame)), label='ChA Real bs')
    ax3.plot(range(len(rec_sig_sing_frame)), np.imag((rec_sig_sing_frame)), label='ChA Imag bs')
    ax3.plot(range(len(rec_sig_sing_frame)), np.abs((rec_sig_sing_frame)), label='ChA abs')
    ax3.legend(fontsize=10)

    ax4 = fig.add_subplot(2, 2, 4)
    ax4.grid(True)
    ax4.set_ylabel('Signal (units)')
    ax4.set_xlabel('Sample index')
    ax4.phase_spectrum(rec_sig)
    ax4.legend(fontsize=10)

    plt.show()

if __name__ == '__main__':
    one_frame_test()
