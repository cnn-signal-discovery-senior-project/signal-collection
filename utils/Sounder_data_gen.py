#!/usr/bin/python3
"""
Sounder_data_gen.py

Create the .bin file to store tx data for the Sounder application
 from pregenerated data signals 


"""

import numpy as np
import h5py
from optparse import OptionParser




def main(dataset_file, file_out):
    filename_tx_samps = ""
    filename_pre_gen_sigs = ""
    for clIdx in [0,1]:
        file_out = "ul_data_t_16QAM_52_64_327_1_10000_A_{}.bin".format(clIdx)
        with h5py.File(dataset_file, 'r') as dataset:
            ul_data = np.array(dataset['generated_data/dataset'][:10000,:], dtype=np.complex64)

            ul_data.tofile(file_out)
    

if __name__ == "__main__":
    parser = OptionParser()
    # Params
    parser.add_option("--dataset",       type="string",       dest="dataset",       default="../IrisUtils/data_in/<filename>.hdf5", help="HDF5 filename to be read in REPLAY mode [default: %default]")
    parser.add_option("--file_out",       type="string",       dest="file_out",       default="../IrisUtils/data_in/<filename>.bin", help="HDF5 filename to be read in REPLAY mode [default: %default]")
    (options, args) = parser.parse_args()

    # Params
    file_in = options.dataset
    file_out = options.file_out

    main(file_in, file_out)